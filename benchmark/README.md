PRINSEQ is another tool used to filtering low complexity reads.  
It filters either using the dust score or entropy.  
it can be installed with `conda install -c bioconda prinseq`  
`nice -20 snakemake --cores 1` can be called afterwards as usual
`fluff.py` can be used to trim resulting files for visualization
