#!/bin/python
"""
This tool will add the Fastq markers to a file with only bare reads
"""
import sys

with open(sys.argv[1], "r") as file:
    a = file.readlines()
if len(sys.argv) > 2:
    with open(sys.argv[2], "r") as file:
        b = file.readlines()
    a = a[: min(len(a), len(b))]
    b = b[: min(len(a), len(b))]
    with open("fluff_" + sys.argv[2], "w") as file:
        for i in b:
            file.write("@SRR1\n" + i + "+SRR1\n" + "A" * (len(i) - 1) + "\n")
with open("fluff_" + sys.argv[1], "w") as file:
    for i in a:
        file.write("@SRR1\n" + i + "+SRR1\n" + "A" * (len(i) - 1) + "\n")
