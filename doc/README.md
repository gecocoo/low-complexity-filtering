**How to generate/work with the Doc:**
* 1. `pip install -U Sphinx astropy-sphinx-theme` - Installs Sphinx and the theme
* 2. `make html` - generates HTML from .rst in source.

**For debugging purpose**
* 1. `sphinx-apidoc -o source ../lcfilter` - optional, generate .rst files from the package. Further documentations can added to these and moved to source.
* 2. `make clean` - clears up artifacts.