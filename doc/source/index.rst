Welcome to LCFilters's documentation!
====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:	


LCFilter is a tool for filtering DNA Reads by their complexities. It can read a FASTQ file (optionally with its Paired file) and calculate each contained read's complexity score based on the distribution of its k-mers (subsequences of length k). Offers functionality to filter DNA Reads by low / high complexity. Reads of input files are divided into high complexity and low complexity output files. It offers multithreading and chunkwise reading and
processing of FASTQ files.

The most important subcommand for filtering Reads is 'process'. Subcommands 'statistics' and 'visualize' can be used to get more information on complexity score distributions.

The Process subcommand will classify all Reads in a FASTQ file (and optionally it's Paired file) as either high or low complexity Reads. Low complexity Reads are written to low.fq with the given output prefix. High complexity Reads are written to high.fq with the given out prefix.In the case of Paired-End files the output is divided into low1.fq and low2.fq / high1.fq and high2.fq respectively, of course with the output prefix. With Paired-End files, there are also output files mixed1.fq and mixed2.fq. These contain Reads that are classified as mixed complexity. Reads in mixed1.fq come from the first input file, Reads in mixed2.fq come from the second input file.

The process and statistics subcommand are receiving their arguments from the main entry point, a command line interface. It's not recommended to import the modules manually. Use the command line interface to operate with the default values. The most common arguments are:

- **--fastq**		input FASTQ file (only for process and statistics)
- **--stats**		determine JSON file (only for visualize)
- **--out**		determine output dictionary
- **--threadcount**	determine the number of threads that should be used
- **--kmer** 		determine the length of the k-mers
- **--pairs**		determine if the FASTQ file is Single- or Paired-End
- **--rev**		determine if reverse complements should be contained


How to install lcfilter:
========================
Mac/Linux:
**********

- Install Anaconda or miniconda
- conda env create -f environment.yml (Might throw error message, because shell isnt initialized: conda init -help)
- Create folder 'raw' for FASTQ Files.
- For example place SRR9130495_1.fastq, SRR9130495_2.fastq in it (https://www.ebi.ac.uk/ena/browser/view/SRR9130495)
- Create folder 'output'

**To start the tool:**

- conda activate lcfilter
- python setup.py develop

**With Snakemake:**

- snakemake --cores all

**Or via shell command:**

- lcfilter process -q path_to_file
- use lcfilter -h or lcfilter process -h for more information on how to use the tool

Windows:
********

- Install Anaconda or miniconda
- conda create -n lcfilter python=3.8 numpy numba matplotlib
- Create folder 'raw' for FASTQ Files.
- For example place SRR9130495_1.fastq, SRR9130495_2.fastq in it (https://www.ebi.ac.uk/ena/browser/view/SRR9130495)
- Create folder 'output'

**To start the tool:**

- conda activate lcfilter
- python setup.py develop
- lcfilter process -q path_to_file
- use lcfilter -h or lcfilter process -h for more information how to use the tool

.. TODO:
.. Are there limitations?
.. Which part of the code is parallelized in which way?

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
