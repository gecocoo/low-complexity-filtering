REFPATH = "ref/"
RAWPATH = "raw/"
RESULTPATH = "results/"
OUTPUTPATH = "output/"

MOUSE_EXOMS = ["SRR9130495_1.fastq","SRR9130495_2.fastq"]
#MOUSE_EXOMS_GZ = ["SRR9130495_1.gz","SRR9130495_2.gz"]

IDS = [i[:-8] for i in MOUSE_EXOMS[::2]]
#IDS = [i[:-5] for i in MOUSE_EXOMS_GZ[::2]]

KS = [5]
CONFIGS = [(5,0.85,0.05,0.15)]
PLOTS = ['scatter','hist2d','dist','classification','max k-mer score','unique k-mer score','unique-max score']

rule all:
    input:
        expand(RESULTPATH + "{sample}_k-{a[0]}_u-{a[1]}_m-{a[2]}_mi-{a[3]}_classify.log", sample=IDS, a=CONFIGS),
        expand(RESULTPATH + "{sample}_k-{k}_{plots}.png", sample=IDS, k=KS,plots=PLOTS),
        expand(RESULTPATH + "{sample}-t8.log", sample=IDS),
    
rule process_mouse_exomes:
    input:
        #in1 = RAWPATH + "{sample}_1.gz",
        in1 = RAWPATH + "{sample}_1.fastq",
        #in2 = RAWPATH + "{sample}_2.gz",
        in2 = RAWPATH + "{sample}_2.fastq",
    log:
        RESULTPATH + "{sample}-t{t}.log"
    benchmark:
        RESULTPATH + "{sample}-t{t}.benchmark"
    params:
        outpath=RESULTPATH
    shell:
        "(time lcfilter process -T {wildcards.t} -k 5 --fastq {input.in1} -o {params.outpath}) &> {log}"

rule calculate_statistics:
    input:
        in1 = RAWPATH + "{sample}_1.fastq",
        in2 = RAWPATH + "{sample}_2.fastq",
    output:
        RESULTPATH + "{sample}_k-{k}_stats.pickle"
    params:
        outpath=RESULTPATH
    shell:
        "lcfilter statistics -T 8 --fastq {input.in1} -k {wildcards.k} --pairs {input.in2} -o {params.outpath}"

rule visualize_statistics:
    input:
        RESULTPATH + "{sample}_k-{k}_stats.pickle"
    output:
        expand(RESULTPATH + "{{sample}}_k-{{k}}_{plots}.png",plots=PLOTS)
    params:
        outpath=lambda w, input: input[0].split('/')[0]
    shell:
        "lcfilter visualize -s {input} -o {params.outpath}"

rule classify_statistics:
    input:
        RESULTPATH + "{sample}_k-{k}_stats.pickle"
    log:
        RESULTPATH + "{sample}_k-{k}_u-{unique}_m-{max}_mi-{minus}_classify.log"
    script:
        "scripts/count_classification.py"

rule visualize_splitfiles:
    #This rule serves only debugging purposes and can only be applied manually
    input:
        RESULTPATH + "{sample}_k-{k}_stats.pickle"
    output:
        RESULTPATH + "{sample}_k-{k}_compare_source.png"
    script:
        "scripts/visualize_splitfiles.py"