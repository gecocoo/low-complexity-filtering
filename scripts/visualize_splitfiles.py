import pickle
import numpy as np
from lcfilter.visualize import split_file_scatter

with open(snakemake.input[0], "rb") as file:
    j = pickle.load(file)
    # range_unique=[0.7,1]
    # range_max=[0.8,1]
    default_out = snakemake.output[0]
    print(default_out)
    split_file_scatter(j, default_out)
