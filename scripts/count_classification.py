import pickle
import numpy as np
from lcfilter.filter import filter_precomputed

with open(snakemake.input[0], "rb") as file:
    j = pickle.load(file)
    t_uni = float(snakemake.wildcards.unique)
    t_max = float(snakemake.wildcards.max)
    t_min = float(snakemake.wildcards.minus)
    result = {
        "unique": [0, 0, 0],
        "max": [0, 0, 0],
        "and": [0, 0, 0],
        "minus": [0, 0, 0],
    }
    for i in range(len(j["unique"][0])):
        check1 = j["unique"][0][i] >= t_uni
        check2 = j["unique"][1][i] >= t_uni
        if check1 and check2:  # both pass
            result["unique"][0] += 1
        elif not (check1 or check2):  # no pass
            result["unique"][2] += 1
        else:
            result["unique"][1] += 1  # mixed pass
        # assume max_k_inverted was used
        check1 = j["max"][0][i] >= t_max
        check2 = j["max"][1][i] >= t_max
        if check1 and check2:  # both pass
            result["max"][0] += 1
        elif not (check1 or check2):  # no pass
            result["max"][2] += 1
        else:
            result["max"][1] += 1  # mixed pass
        # And version
        check1 = j["unique"][0][i] >= t_uni and j["max"][0][i] <= t_max
        check2 = j["unique"][1][i] >= t_uni and j["max"][1][i] <= t_max
        if check1 and check2:  # both pass
            result["and"][0] += 1
        elif not (check1 or check2):  # no pass
            result["and"][2] += 1
        else:
            result["and"][1] += 1  # mixed pass
        # And version
        check1 = filter_precomputed(j["unique"][0][i], j["max"][0][i], t_min)
        check2 = filter_precomputed(j["unique"][1][i], j["max"][1][i], t_min)
        if check1 and check2:  # both pass
            result["minus"][0] += 1
        elif not (check1 or check2):  # no pass
            result["minus"][2] += 1
        else:
            result["minus"][1] += 1  # mixed pass
    with open(snakemake.log[0], "w") as file:
        file.write(str(result))
        print(result)
        a = "Unique H: {} M: {} L: {}\nMax H: {} M: {} L: {}\nAnd H: {} M: {} L: {}\nMinus t={} H: {}, M: {} L: {}".format(
            result["unique"][0] / np.sum(result["unique"]),
            result["unique"][1] / np.sum(result["unique"]),
            result["unique"][2] / np.sum(result["unique"]),
            result["max"][0] / np.sum(result["max"]),
            result["max"][1] / np.sum(result["max"]),
            result["max"][2] / np.sum(result["max"]),
            result["and"][0] / np.sum(result["and"]),
            result["and"][1] / np.sum(result["and"]),
            result["and"][2] / np.sum(result["and"]),
            t_min,
            result["minus"][0] / np.sum(result["minus"]),
            result["minus"][1] / np.sum(result["minus"]),
            result["minus"][2] / np.sum(result["minus"]),
        )
        print(a)
        file.write("\n" + a)
