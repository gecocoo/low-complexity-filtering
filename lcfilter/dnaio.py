""" I/O for FASTQ File
Offers functionalty for reading a FASTQ file.
This is possible on Single-Read and Paired-End FASTQ files.

This file requires 'numpy' and 'numba' to be installed on the device it is going
to be used on.

This file can be imported by another .py file and contains the following
functions:

    * _check_format - Checks buffer if each contained record fits the FASTQ format.
    * _find_fastq_seqmarks - Finds the start and end positions of each line in given buffer.
    * fastq_chunks - Writes the FASTQ file into a read- and writeable byte buffer. Also saves borders for each read and the sequence, which is contained in the Read, written in the buffer.
    * fastq_chunks_paired - Writes both FASTQ file each into a read- and writeable byte buffer. Also saves borders for each Read and the sequence, which is contained in the Read, written in the buffer.
    
"""

import io
import warnings
import numpy as np
from numba import njit, uint8, int32


@njit(nogil=True)
def _check_format(m, buf, linemarks):
    """
    Checks if the already written Reads in the buffer fit the FASTQ format.
    In the FASTQ format every first line of a record starts with '@', while every third line starts with '+'.
    The sequence, which is meant to be processed, should not start with '@' or '+'.

    Parameters
    ----------
    m : int
        Number of complete Reads in the buffer
    buf : numpy array of type np.uint8
        Buffer containing each character of the lines, whose borders need to be found
    linemarks : 2-dimensional numpy array of type np.int32
        The borders of each line are written into this array.

    Returns
    -------
    Bool whether the records, which are already written into the buffer, fit the FASTQ format
    """
    i = 0
    while i < m:
        if buf[linemarks[i, 2]] != 64:
            return False
        if buf[linemarks[i, 0]] == 64 or buf[linemarks[i, 0]] == 43:
            return False
        if buf[linemarks[i, 1] + 1] != 43:
            return False
        i += 1
    return True


@njit(nogil=True)
def _find_fastq_seqmarks(buf, linemarks):
    """
    Find start and end positions of lines in buf.

    Parameters
    ----------
    buf : numpy array of type np.uint8
        Buffer containing each character of the lines, whose borders need to be found
    linemarks : 2-dimensional numpy array of type np.int32
        The borders of each line are written into this array.

    Returns
    -------
    Pair (m, nxt), where:
        m : uint8[:]
            number of sequences processed
        nxt : int32[:,:]
            position at which to continue processing in the next iteration (start of new entry, '@')
            determined by searching a new line ('\n' as byte is '10')
    """
    n = buf.size
    if n == 0:
        return 0, 0

    M = linemarks.shape[0]
    i = 0  # index of which byte is looked at in buffer
    m = -1  # number of current record we're in
    nxt = 0  # byte of beginning of last record
    line = 0  # current line in FASTQ record (0,1,2,3)
    # find start of current line
    while True:
        if buf[i] == 10:
            i += 1
            if line == 0:
                linemarks[m, 3] = i
            if i >= n:
                # we did not find valid record start
                if line == 0:
                    m += 1
                    nxt = n
                return m, nxt
        if line == 0:
            m += 1
            nxt = i
            linemarks[m, 2] = i
            if m >= M:
                return M, nxt
        elif line == 1:
            linemarks[m, 0] = i
        # find end of current line
        while buf[i] != 10:
            i += 1
            if i >= n:
                # we did not find the end of the line before the buffer was exhausted
                # we cannot set linemarks[m][1]
                return m, nxt
        if line == 1:
            linemarks[m, 1] = i
        line = (line + 1) % 4


def fastq_chunks(files, bufsize=2 ** 23, maxreads=(2 ** 23) // 200):
    """
    Yield all chunks from a list or tuple of FASTQ files.

    Parameters
    ----------
    files : tuple/list of strings OR single string
        Path(s) to the input file(s)
    bufsize : int
        Size of created Buffer
    maxreads : int
        Maximum number of Reads, which can be stored in the buffer

    Returns
    -------
    A chunk is a pair (buffer, linemarks), where
    - buffer is readable/writable byte buffer
    - buffer[linemarks[i,0]:linemarks[i,1]] contains the i-th sequence
      of the chunk; buffer[linemarks[i,2]:linemarks[i,3]] contains the i-th Read
      of the chunk
    - The part of linemarks that is returned is such that
      linemarks.shape[0] is at most maxreads and equals the number of Reads.
    CAUTION: If bufsize is very small, such that not even a single FASTQ entry
      fits into the buffer, it will appear that the buffer is empty.
    """
    # defaults are good for single-threaded runs; multiply by #threads.
    if not (isinstance(files, tuple) or isinstance(files, list)):
        files = (files,)  # single file?
    if bufsize > 2 ** 30:
        raise ValueError("require bufsize <= 1 GiB (2**30)")
    linemarks = np.empty((maxreads, 4), dtype=np.int32)
    buf = np.empty(bufsize, dtype=np.uint8)
    m = 0
    for filename in files:
        with io.BufferedReader(io.FileIO(filename, "r"), buffer_size=bufsize) as f:
            prev = 0
            while True:
                read = f.readinto(buf[prev:])
                if read == 0:
                    if (
                        not linemarks[m, 0]
                        == linemarks[m, 1]
                        == linemarks[m, 2]
                        == linemarks[m, 3]
                        == 0
                    ):
                        warnings.warn(
                            "detected possible data loss, check for cut-off record or missing new line at the end of file"
                        )
                    break
                available = prev + read  # number of bytes available
                linemarks.fill(0)
                m, cont = _find_fastq_seqmarks(buf[:available], linemarks)
                if not _check_format(m, buf[:available], linemarks):
                    raise RuntimeError(f"detected non-fitting format of input file")
                if m <= 0:
                    raise RuntimeError(f"no complete records for bufsize {bufsize}")
                chunk = (buf, linemarks[:m])
                yield chunk
                prev = available - cont
                if prev > 0:
                    buf[:prev] = buf[cont:available]
                assert prev < cont


def fastq_chunks_paired(pair, bufsize=2 ** 23, maxreads=2 ** 23 // 200):
    """
    Yield all chunks from a list or tuple of paired FASTQ files.

    Parameters
    ----------
    pair : tuple of (tuple/list of strings OR single string)
        Pair containing the Path(s) to the paired input files
    bufsize : int
        Size of created Buffer
    maxreads : int
        Maximum number of Reads, which can be stored in the buffer

    Returns
    -------
    A chunk is a pair (buffer, linemarks), where
    - buffer is readable/writable byte buffer
    - buffer[linemarks[i,0]:linemarks[i,1]] contains the i-th sequence
      of the chunk; buffer[linemarks[i,2]:linemarks[i,3]] contains the i-th Read
      of the chunk
    - The part of linemarks that is returned is such that
      linemarks.shape[0] is at most maxreads and equals the number of Reads.
    CAUTION: If bufsize is very small, such that not even a single FASTQ entry
      fits into the buffer, it will appear that  the buffer is empty.
    """
    # defaults are good for single-threaded runs; multiply by #threads.
    (files1, files2) = pair
    if not (isinstance(files1, tuple) or isinstance(files1, list)):
        files1 = (files1,)
    if not (isinstance(files2, tuple) or isinstance(files2, list)):
        files2 = (files2,)
    linemarks1 = np.empty((maxreads, 4), dtype=np.int32)
    linemarks2 = np.empty((maxreads, 4), dtype=np.int32)
    buf1 = np.empty(bufsize, dtype=np.uint8)
    buf2 = np.empty(bufsize, dtype=np.uint8)
    m1 = 0
    m2 = 0
    if len(files1) != len(files2):
        raise RuntimeError(f"different numbers of fastq files")
    for i in range(len(files1)):
        with io.BufferedReader(
            io.FileIO(files1[i], "r"), buffer_size=bufsize
        ) as f1, io.BufferedReader(
            io.FileIO(files2[i], "r"), buffer_size=bufsize
        ) as f2:
            prev1 = 0
            prev2 = 0
            while True:
                read1 = f1.readinto(buf1[prev1:])
                read2 = f2.readinto(buf2[prev2:])
                if read1 == 0 or read2 == 0:
                    if not (
                        linemarks1[m1, 0]
                        == linemarks1[m1, 1]
                        == linemarks1[m1, 2]
                        == linemarks1[m1, 3]
                        == 0
                        and linemarks2[m2, 0]
                        == linemarks2[m2, 1]
                        == linemarks2[m2, 2]
                        == linemarks2[m2, 3]
                        == 0
                    ):
                        warnings.warn(
                            "detected possible data loss, check for cut-off record or missing new line at the end of file"
                        )
                    break
                available1 = prev1 + read1
                available2 = prev2 + read2
                linemarks1.fill(0)
                linemarks2.fill(0)
                m1, cont1 = _find_fastq_seqmarks(buf1[:available1], linemarks1)
                m2, cont2 = _find_fastq_seqmarks(buf2[:available2], linemarks2)
                if not (
                    _check_format(m1, buf1[:available1], linemarks1)
                    and _check_format(m2, buf2[:available2], linemarks2)
                ):
                    raise RuntimeError(f"detected non-fitting format of input file")
                if m1 <= 0 or m2 <= 0:
                    raise RuntimeError(f"no complete records for bufsize {bufsize}")
                assert m1 == m2
                chunk = (buf1, linemarks1[:m1], buf2, linemarks2[:m2])
                yield chunk
                prev1 = available1 - cont1
                prev2 = available2 - cont2
                if prev1 > 0:
                    buf1[:prev1] = buf1[cont1:available1]
                if prev2 > 0:
                    buf2[:prev2] = buf2[cont2:available2]
                assert prev1 < cont1
                assert prev2 < cont2
