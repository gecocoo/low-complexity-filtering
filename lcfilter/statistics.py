""" FASTQ Statistics File

This Module is used to collect some basic Information about reads in FASTQ files.

It works by taking in all the same FASTQ files as the process submodule but instead of filtering the files it just records the max-kmer and unique-kmer scores,
which are used by the filter and writes them out to a file.

That file will by default be give as a pickle file due to it's size and can be used to manually analyse filter the FASTQ file.
The file will contain a dict with at least the fields 'max' and 'unique' containing two arrays/lists each with the scores for the reads in both input files (if it is only single read mode the second list is zero).
The dict also contains the key 'counts', which counts the occurences of all k-mers in all reads in both files.

This file can also be analysed with the visualization submodule.

The fast that this module provides a subcommand seperate from the visualization is donw to saving re-computation on multiple different runs for visualizations.

None of this files function should be used directly, but only through calling lfcilter statistics [...].

"""

import numpy as np
from numba import njit, jit, uint32, prange
import datetime

from concurrent.futures import Executor, ThreadPoolExecutor, as_completed
from math import ceil

from lcfilter.dnaio import fastq_chunks, fastq_chunks_paired
from lcfilter.kmer import process_kmers
from lcfilter.filter import unique_kmers, max_kmer, filter_dust, filter_entropy


def make_process_read_from_fastq(
    threads,
    pairs,
    k,
    rev_compl,
    complexities,
    output,
    split,
    outputjson=False,
    bufsize=2 ** 23,
    chunkreads=2 ** 23 // 200,
):
    """Generate a process function for FASTQ file reading.

    Parameters
    ----------
    threads : int
        Number of Threads
    pairs : file
        Second inputfile which is given as Commandline Argument
    k : int
        Length of each k-mer, which is used to specify low or high Complexity
    rev_compl : Bool
        Whether the reverse complement of each kmer should also be considered
    complexities : list of 4-tuple
        List containing the complexities of each read with usage of unique_kmers and max_kmer
        - unique_kmers() value for read of first input file
        - unique_kmers() value for read of second input file
        - max_kmer() value for read of first input file
        - max_kmer() value for read of second input file
    output : String
        Commandline Argument to set the output directory
    split : String
        Name prefix of the output file
    bufsize : int
        Size of created Buffer, which will contain the Reads
    chunkreads : int
        Maximum number of Reads, which can be stored in the buffer
    Returns
    -------
    Generated function.
    """

    @njit(nogil=True)
    def get_border(linemarks, threads):
        """Finds border-indices for each chunk depending on the number of used threads

        Parameters
        ----------
        linemarks : 2-dimensional numpy array of type uint32
            Array containing the start- and end-indices of each Read.
            - linemarks[i][0] Start index of the i-th sequence
            - linemarks[i][1] End index of the i-th sequence
            - linemarks[i][2] Start index of the i-th Read
            - linemarks[i][3] End index of the i-th Read
            - e.g. [54, 155, 0, 260] for a sequence of the length 101
        threads : int
            Number of Threads, which determines in how many parts the chunk has to be cut.

        Returns
        -------
        borders : numpy array of type uint32
            Contains the start- and end-indices for each Thread. The end-index of Thread i is the start-index of Thread i+1.
            - e.g. [  0,  17,  34,  51,  68,  85, 102, 119, 136, 153, 170, 187, 200] for 12 Threads (13 indices)
            - The first thread processes the first 17 of 200 reads/sequences
        """
        borders = np.empty(threads + 1, dtype=uint32)
        for i in range(threads):
            borders[i] = min(ceil(linemarks.shape[0] / threads) * i, linemarks.shape[0])
        borders[threads] = linemarks.shape[0]
        return borders

    @njit
    @process_kmers(k=k, include_rev_compl=rev_compl)
    @njit
    def count_low_k(kmer: int, counter: np.ndarray):
        """Function to be called for each k-mer in a sequence to count the occurrence of each k-mer by the use of
        a decorator.

        Parameters
        ----------
        k : int
           Length of each k-mer.
        include_rev_compl : Bool
            Flag that indicates whether the reverse complement of each k-mer shall be also counted.
        kmer: int
            Encoded representation of the seen k-mer, which shall be counted.
        counter : numpy ndarray
            Array containing the number of each k-mer occurring at the index of the integer encoded k-mer.

        Returns
        -------
        counter : numpy ndarray
            Array containing the number of each k-mer occurring at the index of the integer encoded k-mer.
        """
        counter[kmer] += 1
        return counter

    def processor(buf, linemarks, buf1=None, linemarks1=None):
        """Function for processing of each chunk.

        Parameters
        ----------
        buf : numpy array
            Contains all characters of the first input file as byte.
        linemarks : 2-dimensional numpy array of type uint32
            Array containing the start- and end-indices of each Read of the first input file.
            - linemarks[i][0] Start index of the i-th sequence
            - linemarks[i][1] End index of the i-th sequence
            - linemarks[i][2] Start index of the i-th Read
            - linemarks[i][3] End index of the i-th Read
            - e.g. [54, 155, 0, 260] for a sequence of the length 101
        buf [optional] : numpy array
            Contains all characters of the second input file (if existing) as byte.
        linemarks1 [optional] : 2-dimensional numpy array of type uint32
            Array containing the start- and end-indices of each Read of the second input file (if existing).
            See linemarks for more information.

        Returns
        -------
        Numpy array containing the occurrence of each possible k-mer in this chunk.
        """
        counts = np.zeros((4 ** k), dtype=np.uint32)
        n = linemarks.shape[0]
        u0 = np.zeros(n)
        u1 = np.zeros(n)
        m0 = np.zeros(n)
        m1 = np.zeros(n)
        d0 = np.zeros(n)
        d1 = np.zeros(n)
        e0 = np.zeros(n)
        e1 = np.zeros(n)
        l = np.zeros(n)

        complexities.append((u0, u1, m0, m1, d0, d1, e0, e1, l))

        for i in range(n):
            sq = buf[linemarks[i, 0] : linemarks[i, 1]]
            counter1 = np.zeros((4 ** k), dtype=np.uint32)
            counter1 = count_low_k(sq, args=counter1)

            # measure complexities
            l[i] = np.sum(counter1)
            u0[i] = unique_kmers(counter1)
            m0[i] = max_kmer(counter1)
            d0[i] = filter_dust(counter1)
            e0[i] = filter_entropy(counter1)
            counts += counter1

            # processing read of second file
            if not buf1 is None:
                sq1 = buf1[linemarks1[i, 0] : linemarks1[i, 1]]
                counter2 = np.zeros((4 ** k), dtype=np.uint32)
                counter2 = count_low_k(sq1, args=counter2)

                # measure complexities
                u1[i] = unique_kmers(counter2)
                m1[i] = max_kmer(counter2)
                d1[i] = filter_dust(counter1)
                e1[i] = filter_entropy(counter1)
                counts += counter2
        return counts

    def process_read_from_fastq(fastq):
        with ThreadPoolExecutor(max_workers=threads) as executor:
            """Process function for FASTQ file reading.

            Parameters
            ----------
            fastq : file
                First input file which is given as Commandline Argument

            Returns
            -------
            Numpy array containing the occurrence of each possible k-mer
            """

            """ Initialization of variables """
            counts = np.zeros(4 ** k, np.uint64)

            """ Reading input-files
            depending on the number of input files (1 or 2 files) a reading method is called
            readMethod contains as many chunks as the number of threads being used
            a chunk is a tuple of two arrays
            - the first contains all characters of the file as byte
            - the second is an array with 4 indices:
                - [start of sequence, end of sequence, start of whole entry, end of whole entry]
                - e.g. [ 54, 155, 0, 260] for the first entry of our FASTQ file with a sequence-length of 101
            """
            if pairs:
                readMethod = fastq_chunks_paired(
                    (fastq, pairs),
                    bufsize=bufsize * threads,
                    maxreads=chunkreads * threads,
                )
            else:
                readMethod = fastq_chunks(
                    fastq, bufsize=bufsize * threads, maxreads=chunkreads * threads
                )

            """ Processing depending on mode and paired 
            Each chunk is separated to be processed by the defined number of threads simultaneously.
            If only one file is processed, one BufferedWriter for each low and high complexity is given.
            If paired files are processed, these two BufferedWriter are given for each file and two additional 
            BufferedWriters are given, one for each paired file, to store Reads with mixed complexity.
            """
            for chunk in readMethod:
                borders = get_border(chunk[1], threads)
                if pairs:
                    futures = [
                        executor.submit(
                            processor,
                            chunk[0],
                            chunk[1][borders[i] : borders[i + 1]],
                            chunk[2],
                            chunk[3][borders[i] : borders[i + 1]],
                        )
                        for i in range(threads)
                    ]
                else:
                    futures = [
                        executor.submit(
                            processor,
                            chunk[0],
                            chunk[1][borders[i] : borders[i + 1]],
                        )
                        for i in range(threads)
                    ]
                """ handling of each result """
                for i in as_completed(futures):
                    result = i.result()
                    counts += result

        """ output code """
        import pickle
        import json

        c = dict()
        if outputjson == False:
            c["unique"] = [
                np.concatenate([i[0] for i in complexities]),
                np.concatenate([i[1] for i in complexities]),
            ]
            c["max"] = [
                np.concatenate([i[2] for i in complexities]),
                np.concatenate([i[3] for i in complexities]),
            ]
            c["dust"] = [
                np.concatenate([i[4] for i in complexities]),
                np.concatenate([i[5] for i in complexities]),
            ]
            c["entropy"] = [
                np.concatenate([i[6] for i in complexities]),
                np.concatenate([i[7] for i in complexities]),
            ]
            c["counts"] = counts
            c["lengths"] = np.concatenate([i[8] for i in complexities])
            filename = output + "/" + split + "_k-" + str(k) + "_stats.pickle"
            with open(filename, "wb") as file:
                pickle.dump(c, file)
            return counts
        else:
            c["unique"] = [
                [float(i) for i in np.concatenate([i[0] for i in complexities])],
                [float(i) for i in np.concatenate([i[1] for i in complexities])],
            ]
            c["max"] = [
                [float(i) for i in np.concatenate([i[2] for i in complexities])],
                [float(i) for i in np.concatenate([i[3] for i in complexities])],
            ]
            c["dust"] = [
                np.concatenate([i[4] for i in complexities]),
                np.concatenate([i[5] for i in complexities]),
            ]
            c["entropy"] = [
                np.concatenate([i[6] for i in complexities]),
                np.concatenate([i[7] for i in complexities]),
            ]
            c["counts"] = [int(i) for i in counts]
            c["lengths"] = [
                float(i) for i in np.concatenate([i[8] for i in complexities])
            ]
            filename = output + "/" + split + "_k-" + str(k) + "_stats.json"
            with open(filename, "w") as file:
                json.dump(c, file)
            return counts

    return process_read_from_fastq


def main(args):
    """ main method for processing Reads"""

    now = datetime.datetime.now()
    print(f"#{now:%Y-%m-%d %H:%M:%S}: Begin processing")
    if args.fastq:
        if args.out[-1] == "/":
            output_folder_name = args.out
        else:
            output_folder_name = args.out + "/"

        process_read_from_fastq = make_process_read_from_fastq(
            args.threadcount,
            args.pairs,
            args.kmer,
            False,
            [],
            output_folder_name,
            args.fastq.split("/")[-1].split("_")[0],
            outputjson=args.json,
        )
        counts = process_read_from_fastq(args.fastq)
        now = datetime.datetime.now()
        print(f"#{now:%Y-%m-%d %H:%M:%S}: All Reads processed")
    else:
        assert False
