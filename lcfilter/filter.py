""" FASTQ Filter File

This module contains the statistics and filter functions used to process a FASTQ file or which are collected by the statistics module.

This file can be imported by another .py file and contains the following
functions:
    * filter - the default filter function used in processing
    * filter_precomputed - the filter used when visulaizing already recorded statistics
    * max_kmer - a statistic based on the most frequent k-mer in a read
    * max_kmer_inverted - the same but inverted so a higher number is more complex
    * unique_kmers - a statistic based of the number of different k-mers in a read

"""

import numpy as np
import matplotlib.pyplot as plt
import math

from numba import njit, jit, boolean, int32, float64


@njit(nogil=True)
def filter(counter, threshold=0.15):
    """Determine if a Read is high or low complexity

    Parameters
    ----------
    counter : int[:]
        The numpy array which contains the occurrences of all k-mers
    treshold : float, optional
        The threshold value as a float from which on a Read will be considered highly complex
        Default value is 0.15
    Returns
    -------
    Bool whether the Read is complex or not
    """
    return unique_kmers(counter) - max_kmer(counter) * 1.25 > threshold


def filter_precomputed(uni, max, threshold=0.15):
    """Determine if a Read is high or low complexity by using pre-computed scores.

    Parameters
    ----------
    uni : float
        The unique score of the Read
    max : float
        The max score of the Read
    treshold : float, optional
        The threshold value as a float from which on a Read will be considered highly complex
        Default value is 0.15
    Returns
    -------
    Bool whether the Read is complex or not
    """
    return uni - max * 1.25 > threshold


@njit
def max_kmer(counter):
    """
    Divide the most common k-mer by the total number of k-mers in a Read and normalize it
    returns a number in [0,1] with lower numbers being more complex
    0 means every k-mer occurred only once
    1 means there is only one k-mer
    """

    n = np.sum(counter)
    if n <= 10:  # edge case for small reads
        return 1
    max_k = np.max(counter)
    x = max_k / n
    # 1/n <= x <= 1  <=>
    # 0 <= (n*x-1)/(n-1) <= 1
    return (n * x - 1) / (n - 1)


@njit
def max_kmer_inverted(counter):
    """
    returns 1-max_kmer so higher numbers mean more complexity
    """
    return 1 - max_kmer(counter)


@njit
def unique_kmers(counter):
    """
    Count the number of unique k-mers and divide by the total number occurring k-mers
    returns a number in [0,1] with higher numbers being more complex
    0 means only one k-mer occurs
    1 means every possible k-mer occurs at least once
    """
    uniques = np.sum(counter > 0)
    n = min(counter.shape[0], np.sum(counter))
    if n <= 10:  # edge case for small reads
        return 0
    x = uniques / n
    # 1/n <= x <= 1 <=>
    # 0 <= (nx-1)/(n-1) <= 1
    return (n * x - 1) / (n - 1)


# === unused filters ===

# @njit
# def information(kmers_globals):
#     """
#     Calculates the amount of information for each element in the input array.
#     Returns an equally sized array with information values.
#     """
#     a = kmers_globals.shape[0]
#     p_i = kmers_globals[kmers_globals!=0] / np.sum(kmers_globals)
#     return -np.log(p_i)/np.log(a)


# def filter_entropy_global(kmers_reads, kmers_globals):
#     """
#     Calculates the entropy of the input array kmers_reads with respect to information calculated from kmers_globals.
#     Returns True, if the calculated entropy is >= 1.
#     """
#     p_r = kmers_reads / np.sum(kmers_reads)
#     I_g = information(kmers_globals)
#     I_r = np.sum(p_r * I_g)
#     return I_r >= 1


@njit
def filter_entropy(kmers_reads, threshold=0.42):
    """
    Calculates the local entropy of the input array kmers_reads.
    Returns True, if the calculated entropy is greater than the threshold.
    """
    alphabet_size = kmers_reads.shape[0]
    probabilities = kmers_reads[kmers_reads != 0] / np.sum(kmers_reads)
    information = -np.log(probabilities) / np.log(alphabet_size)
    entropy = np.sum(probabilities * information)
    return entropy >= threshold


@njit
def filter_dust(kmer_reads, threshold=0.08):
    """
    Calculate the score S(a) like DUST for every read
    Originally this is defined for 3-mers
    Returns True if the Score is <length/32
    """
    k = int32(np.log(kmer_reads.shape[0]) / np.log(4))
    num_kmers = np.sum(kmer_reads)
    length_read = num_kmers + k - 1
    _score = np.sum(kmer_reads * (kmer_reads - 1) / 2) / (
        num_kmers - 1
    )  # calculat the score. maximum is num_kmers/2
    score_norm = _score / (num_kmers / 2)  # the normed s is to (0,1]
    # t = (
    #     2 / 32
    # )  # the normed theshold. the original was 2, while the original maximum was 32
    return score_norm <= threshold


factory_dict = {"dust": filter_dust, "entropy": filter_entropy, "uniques": filter}


def filter_factory(filter_id):
    if filter_id in factory_dict.keys():
        return factory_dict[filter_id]

    # standard filter
    return filter
