"""DNA Encoding Function File

This file contains a list of functions that encode various datatypes which might
represent a DNA sequence, as an integer through a couple of translation methods.

The file will always initialize the two global variables
sequence_encode_lookup_array1 and sequence_encode_lookup_array2 which should not
be altered by the user and are required by the functions that use arrays as
translations tools for the encoding.

This file requires 'numpy' and 'numba' to be installed on the device it is going
to be used on.

This file can be imported by another .py file and contains the following
functions:

    * initialize__lookup_arrays - Initializes the two global variables which contain the translation arrays for the encoding.
    * _dna_encode_str_if - Encodes a DNA sequence stored as a string, as an integer using if clauses as a translation tool.
    * _dna_encode_bytes_array - Encodes a DNA sequence stored as bytes, as an integer using an array as a translation tool.
    * _dna_encode_ndarray_array_2_at_a_time - Encodes a DNA sequence stored as a np.ndarray with the dtype np.uint8, as an integer using an array as a translation tool and encoding 2 bases at a time.
    * dna_encode - Encodes a DNA sequence stored as either a string, bytes or a np.ndarray with the dtype np.uint8, as an integer.
"""

import numpy as np
from numba import njit
from typing import Any

# 2 Lookup Arrays where the first one translates singular characters and the second one two at a time:
sequence_encode_lookup_array1 = None
sequence_encode_lookup_array2 = None


def initialize__lookup_arrays():
    """Initializes the two global variables which contain the translation arrays for the encoding.

    Initializes the np.ndarray sequence_encode_lookup_array1 with the size 256 and type np.uint8.
    Every index stores the value 255 indicating that no base is being maped onto.
    The index ord("A") = 65 stores the 2 bit encoding of the base A.
    The index ord("C") = 67 stores the 2 bit encoding of the base C.
    The index ord("G") = 71 stores the 2 bit encoding of the base G.
    The index ord("T") = 84 stores the 2 bit encoding of the base T.

    Initializes the np.ndarray sequence_encode_lookup_array2 with the size 65536 and type np.uint8.
    Every Index stores 0, 1 or 2 encoded bases which are encoded in the following way:
    Only the first 6 bits get used.
    The bits 0b11_00_00 store the number of encoded bases.
    The bits 0b00_00_11 store the first base if 1 or 2 bases are encoded.
    The bits 0b00_11_00 store the second base if 2 bases are encoded.
    """

    global sequence_encode_lookup_array1
    global sequence_encode_lookup_array2
    sequence_encode_lookup_array1 = np.full((256), 255, dtype=np.uint8)
    sequence_encode_lookup_array2 = np.full((256 * 256), 0, dtype=np.uint8)

    sequence_encode_lookup_array1[65] = 0b00  # A
    sequence_encode_lookup_array1[67] = 0b01  # C
    sequence_encode_lookup_array1[71] = 0b10  # G
    sequence_encode_lookup_array1[84] = 0b11  # T
    sequence_encode_lookup_array1[10] = 255  # \n

    # Encodes the dna bases by:
    # using the two bits 0b00_00_11 for the encoding of the first base,
    # using the two bits 0b00_11_00 for the encoding of the second base,
    # using the two bits 0b11_00_00 fot the encoding of the number of bases that where encoded:
    #
    # This means that the majority of values in the array is encoded with 0,

    for index in range(256):
        # Sets the translation where there is only one base:
        # First for the one where there is one 'A':
        sequence_encode_lookup_array2[(256 * 65) + index] = 0b01_00_00
        sequence_encode_lookup_array2[(256 * index) + 65] = 0b01_00_00
        # Then for the one where there is one 'C':
        sequence_encode_lookup_array2[(256 * 67) + index] = 0b01_00_01
        sequence_encode_lookup_array2[(256 * index) + 67] = 0b01_00_01
        # Then for the one where there is one 'G':
        sequence_encode_lookup_array2[(256 * 71) + index] = 0b01_00_10
        sequence_encode_lookup_array2[(256 * index) + 71] = 0b01_00_10
        # Then for the one where there is one 'T':
        sequence_encode_lookup_array2[(256 * 84) + index] = 0b01_00_11
        sequence_encode_lookup_array2[(256 * index) + 84] = 0b01_00_11

    # Then for the combinations where there are two bases:
    for firstCharacter in range(4):
        for secondCharacter in range(4):
            index = 0
            value = 0

            # Initializes the two lower bits and defines the position partially:
            if firstCharacter == 0:  # A
                index = 65
                value = 0b00_00
            elif firstCharacter == 1:  # C
                index = 67
                value = 0b01_00
            elif firstCharacter == 2:  # G
                index = 71
                value = 0b10_00
            elif firstCharacter == 3:  # T
                index = 84
                value = 0b11_00

            # Initializes the two upper bits and defines the position partially:
            if secondCharacter == 0:  # A
                index += 256 * 65
                value += 0b00_00
            elif secondCharacter == 1:  # C
                index += 256 * 67
                value += 0b00_01
            elif secondCharacter == 2:  # G
                index += 256 * 71
                value += 0b00_10
            elif secondCharacter == 3:  # T
                index += 256 * 84
                value += 0b00_11

            # Finally encodes the fact that all these 16 combinations have 2 encoded bases:
            value = value | 0b10_00_00

            # Sets the value at the given index:
            sequence_encode_lookup_array2[index] = value


# Initialzes both lookup arrays:
initialize__lookup_arrays()


@njit
def _dna_encode_str_if(seq: np.ndarray) -> int:
    """Encodes a DNA sequence stored as a string, as an integer using if clauses as a translation tool.

    The character "A" gets encoded as the two bits 0b00.
    The character "C" gets encoded as the two bits 0b01.
    The character "G" gets encoded as the two bits 0b10.
    The character "T" gets encoded as the two bits 0b11.
    Every other character gets ignored.

    Examples
    --------

    >>> _dna_encode_str_if("ACGT")
    27

    >>> _dna_encode_str_if("GATTACA")
    9156

    Parameters
    ----------
    seq : str
        The string that is supposed to be encoded.

    Returns
    -------
    int
        The encoded integer which uses 2 bits per encoded base.
    """

    # Starts out with a 0 and encodes each base seperatley and shifts it 2 bits
    # further to the left every time:
    res = 0

    for character in seq:
        if character == "A":  # A
            res = (res << 2) | 0b00
        elif character == "C":  # C
            res = (res << 2) | 0b01
        elif character == "G":  # G
            res = (res << 2) | 0b10
        elif character == "T":  # T
            res = (res << 2) | 0b11

    return res


@njit
def _dna_encode_bytes_array(seq: bytes) -> int:
    """Encodes a DNA sequence stored as bytes, as an integer using an array as a translation tool.

    The byte-character "A" gets encoded as the two bits 0b00.
    The byte-character "C" gets encoded as the two bits 0b01.
    The byte-character "G" gets encoded as the two bits 0b10.
    The byte-character "T" gets encoded as the two bits 0b11.
    Every other byte-character gets ignored.

    Examples
    --------

    >>> _dna_encode_bytes_array(b"ACGT")
    27

    >>> _dna_encode_bytes_array(b"GATTACA")
    9156

    Parameters
    ----------
    seq : bytes
        The bytes that are supposed to be encoded.

    Returns
    -------
    int
        The encoded integer which uses 2 bits per encoded base.
    """

    # Starts out with a 0 and encodes each base seperatley and shifts it 2 bits
    # further to the left every time:
    res = 0
    for character in seq:
        currentMappingResult = sequence_encode_lookup_array1[(character)]
        if currentMappingResult != 255:
            res = (res << 2) | currentMappingResult

    return res


@njit
def _dna_encode_ndarray_array_2_at_a_time(seq: np.ndarray) -> int:
    """Encodes a DNA sequence stored as a np.ndarray with the dtype np.uint8, as an integer using an array as a translation tool and
    encoding 2 bases at a time.

    The np.uint8 ord("A") = 65 gets encoded as the two bits 0b00.
    The np.uint8 ord("C") = 67 gets encoded as the two bits 0b01.
    The np.uint8 ord("G") = 71 gets encoded as the two bits 0b10.
    The np.uint8 ord("T") = 84 gets encoded as the two bits 0b11.
    Every other number gets ignored.

    Examples
    --------

    >>> _dna_encode_ndarray_array_2_at_a_time(np.array([i for i in b"ACGT"], dtype=np.uint8))
    27

    >>> _dna_encode_ndarray_array_2_at_a_time(np.array([i for i in b"GATTACA"], dtype=np.uint8))
    9156

    Parameters
    ----------
    seq : np.ndarray
        The np.ndarray with the dtype np.uint8 that is supposed to be encoded.

    Returns
    -------
    int
        The encoded integer which uses 2 bits per encoded base.
    """

    # Array reference to the 16-bit integer version of the array:
    local_array = None
    sequence_length = len(seq)
    # The resulting integer:
    res = 0

    # Checks whether the length of the sequence can be divided by 2:
    if (sequence_length & 1) == 1:
        # If not the first character gets translated seperatley:
        current_mapping_result = sequence_encode_lookup_array1[seq[0]]

        if current_mapping_result != 255:
            res = current_mapping_result
        local_array = seq[1:].view(np.uint16)
    else:
        local_array = seq.view(np.uint16)

    # Translates all the uint8s where 2 can be translated at the same time:
    for character in local_array:
        currentMappingResult = sequence_encode_lookup_array2[character]

        # Ignores characters that are not A, C, G or T which are indicated by there
        # not being a 2 encoded in the 0b11_00_00 bits:
        number_of_encoded_bases = (currentMappingResult & 0b11_00_00) >> 4
        # Shifts by the number of encoded bases times 2:
        res = res << number_of_encoded_bases * 2
        # And finally ors in the result of the translation into the final result:
        res = res | (currentMappingResult & ((1 << (number_of_encoded_bases * 2)) - 1))

    return res


def dna_encode(seq: Any) -> int:
    """Encodes a DNA sequence stored as either a string, bytes or a np.ndarray with the dtype np.uint8, as an integer.

    If a string is passed in for the seq:
    The character "A" gets encoded as the two bits 0b00.
    The character "C" gets encoded as the two bits 0b01.
    The character "G" gets encoded as the two bits 0b10.
    The character "T" gets encoded as the two bits 0b11.
    Every other number gets ignored.

    If bytes are passed in for the seq:
    The character "A" gets encoded as the two bits 0b00.
    The character "C" gets encoded as the two bits 0b01.
    The character "G" gets encoded as the two bits 0b10.
    The character "T" gets encoded as the two bits 0b11.
    Every other number gets ignored.

    If a np.ndarray with the dtype np.uint8 is passed in for the seq:
    The np.uint8 ord("A") = 65 gets encoded as the two bits 0b00.
    The np.uint8 ord("C") = 67 gets encoded as the two bits 0b01.
    The np.uint8 ord("G") = 71 gets encoded as the two bits 0b10.
    The np.uint8 ord("T") = 84 gets encoded as the two bits 0b11.
    Every other number gets ignored.

    Examples
    --------

    >>> dna_encode("GATTACA")
    9156

    >>> dna_encode(b"GATTACA")
    9156

    >>> dna_encode(np.array([i for i in b"GATTACA"], dtype=np.uint8))
    9156

    Parameters
    ----------
    seq : Any
        The object that is of one of the types string, bytes or np.ndarray that is supposed to be encoded.

    Raises
    ------
    NotImplementedError
        If the parameter is not of any of the supported types string, bytes or np.ndarray.

    Returns
    -------
    int
        The encoded integer which uses 2 bits per encoded base.
    """

    if isinstance(seq, str):
        return _dna_encode_str_if(seq)
    elif isinstance(seq, bytes):
        return _dna_encode_bytes_array(seq)
    elif isinstance(seq, np.ndarray):
        return _dna_encode_ndarray_array_2_at_a_time(seq)
    else:
        raise NotImplementedError(f"No method available for {type(seq)}")
