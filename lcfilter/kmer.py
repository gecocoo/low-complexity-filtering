"""K-Mer Processing File

Offers several functions for encoding, decoding and calculating of k-mers on DNA data.
This is possible either on a small scale and directly in a terminal or larger scale with input/output files.

The file will always initialize the global variable byte_rev_table which should not be altered by the user
and is required by the functions that calculate the reverse complement of a k-mer integer encoding.

This file requires 'numpy' and 'numba' to be installed on the device it is going
to be used on.

This file can be imported by another .py file and contains the following
functions:

    * create_byte_rev_table - Creates an np.ndarray that stores the reverse complement of each integer encoded k-mer at the integers index in the array.
    * make_kmer_processor - Creates an np.ndarray that stores the reverse complement of each integer encoded k-mer at the integers index in the array.
    * comp - Calculates the complement of the integer encoded `k`-mer `x` .
    * rev_compl - Compute the reverse complement of the integer encoded `k`-mer `x` by only using integer operations.
    * _decode - Decodes the passed in integer encoded k-mer using the mapping_dict and returns it as a np.ndarray.
    * dna_decode - Decodes an integer to a DNA sequence as a readable string.


It contains the following classes :

    * process_kmers - Decorater Class that works as a wrapper around the make_kmer_processor function and allows the decorated function to be called for every k-mer in a sequence that gets passed to the resulting decorated function.
"""

__author__ = "Elias Kuthe, Niklas Domagalla, Erik Rieping, Dennis Bednarek"
import numpy as np
from numba import njit
from typing import Callable, TypeVar
from lcfilter.dna_encode import (
    dna_encode,
    sequence_encode_lookup_array1,
    _dna_encode_ndarray_array_2_at_a_time,
)

T = TypeVar("T")

byte_rev_table = None


def create_byte_rev_table() -> np.ndarray:
    """Creates an np.ndarray that stores the reverse complement of each integer encoded k-mer at the integers index in the array.

    For example at the index of the integer Encoded dna sequence "ATGC" the integer encoded sequence "GCAT" will be stored.
    (Note this only works because "ATGC" takes up exactly one byte of space, if you want to calculate the reverse complement of any integer
    encoded dna sequence use the function rev_compl)

    Returns
    -------
    np.ndarray
        The np.ndarray that can be used to translate integer encoded dna sequences that are one byte long into their reverse complement.
    """
    table = np.zeros((256), dtype=np.uint8)

    # Reverses the bytes:
    for i in range(256):
        # Creates a helper byte and switches every individual bit:
        local_byte = 0
        for j in range(4):
            local_byte = local_byte | ((i & (3 << (2 * j))) >> (2 * j)) << (6 - (2 * j))
        table[i] = local_byte

    return table


# Allways creates the byte reverse array:
byte_rev_table = create_byte_rev_table()


def make_kmer_processor(
    f: Callable[[int, T], T],
    k: int,
    include_rev_compl: bool,
) -> Callable[[np.ndarray, T], T]:
    """Returns a function that will call the passed in function f for every k-mer with the integer encoded k-mer as a parameter on call.

    Can be used as a decorator for a function f with the format f(int, T) -> T.

    Examples
    --------

    >>> def concatinate_kmers_in_one_line(kmer, args): return args+" "+str(kmer)
    >>> processor = make_kmer_processor(concatinate_kmers_in_one_line, 3, False)
    >>> processor((np.array([i for i in b"ACGTA"], dtype=np.uint8)), "") ' 6 27 44'

    Parameters
    ----------
    f : Callable[[int, T], T]
        The function that is supposed to be called for every k-mer.
    k : int
        The length of each k-mer.
    include_rev_compl [optional]: bool
        Flag that indicates whether f should be called not only
        with all the k-mers of the sequence arr but also with all
        of those k-mers reverse complements.
    Raises
    ------
    ValueError
        If a k is passes in that is smaller than 1.

    Returns
    -------
    Callable[[np.ndarray, T], T]
        A function that can be called with an np.ndarray that contains a dna sequence and an initial
        object and which will execute the function f for every k-mer (possibly also for every reverse
        complement).
    """

    # Check whether k is 1 or bigger:
    if k <= 0:
        raise ValueError("Error: The parameter k must be 1 or bigger!")

    def kmer_processor(arr: np.ndarray, args: T = None) -> T:
        """The inner function that gets returned by the make_kmer_processor and that will call f for every k-mer.

        Parameters
        ----------
        arr : np.ndarray
            The dna sequence the k-mers are going to be processed with using the function f.
        args : T
            The initial value of the result value whichs gets passed to into the function fasta
            alongside the current k-mer every call and set to the result of the last call.
        Returns
        -------
        T
            The final value the args hold.
        """

        # Check whether the array is longer than a k-mer:
        if k > len(arr):
            return args
            # raise ValueError(
            #     "Error: The array arr must at least be as long as a single k-mer!"
            # )

        a = _dna_encode_ndarray_array_2_at_a_time(arr[0:k])
        args = f(a, args)
        if include_rev_compl:
            args = f(rev_compl(a, k), args)
        for i in range(k, len(arr)):
            elem_int = sequence_encode_lookup_array1[arr[i]]
            if elem_int <= 0b11:
                a = a & (1 << 2 * (k - 1)) - 1  # remove first two bits
                a = (a << 2) | elem_int  # add two bits for the next base
                args = f(a, args)
                if include_rev_compl:
                    args = f(rev_compl(a, k), args)
        return args

    kmer_processor.__doc__ = (
        f.__doc__ if hasattr(f, "__doc__") else "" "\n TODO: how to call this"
    )
    return kmer_processor


@njit
def comp(x: int, k: int) -> int:
    """Calculates the complement of the integer encoded `k`-mer `x` .

    Examples
    --------

    >>> encoded_comp = comp(dna_encode("GATTACA"), 7)
    >>> dna_decode(encoded_comp, 7)
    'CTAATGT'

    Parameters
    ----------
    x : int
        The integer encoded dna segment the complement is supposed to be calculated for.
    k : int
        The length of the segment in bases (one base is stored in 2 bits).
    Returns
    -------
    int
        The integer encoded complement of the passed in dna segment.
    """

    return x ^ ((1 << 2 * k) - 1)


@njit
def rev_compl(x: int, k: int) -> int:
    """Compute the reverse complement of the integer encoded `k`-mer `x` by only using integer operations.

    Examples
    --------

    >>> dna_decode(rev_compl(dna_encode("GATTACA"), 7), 7)
    'TGTAATC'

    Parameters
    ----------
    x : int
        The integer encoded dna sequence the reverse complement is going to be calculated for.
    k : int
        The length of the segment in bases (one base is stored in 2 bits).
    Returns
    -------
    int
        The integer encoded reverse complement of the given dna sequence.
    """

    compl = comp(x, k)
    rev = 0

    # Calculates the number of bytes the k-mer is getting stored in:
    byte_count = k >> 2  # Times 2 bit per k, Divided by 8 bit per byte
    bits_under_clean_byte = 0

    # If the the number of bits does not fit into bytes neatly byte_count gets 'rounded up':
    if ((k << 1) & 7) != 0:
        byte_count += 1
        bits_under_clean_byte = 8 - ((k << 1) & 7)

    # Reverses each of the byte_count bytes internally and then reverses the bytes themselves:
    for byte_index in range(byte_count):
        # Isolates the current byte and reverses it:
        current_reversed_byte = byte_rev_table[((compl >> (8 * byte_index)) & 255)]
        # Now it moves the byte to its corrext reversed position:
        rev = rev | (current_reversed_byte << (8 * (byte_count - 1 - byte_index)))

    # Finally cuts of the bits_under_clean_byte 0 bits that are at the right of the number and dont belong to the kmer:
    rev = rev >> bits_under_clean_byte

    return rev


class process_kmers:
    """Decorater Class that works as a wrapper around the make_kmer_processor function and allows the
    decorated function to be called for every k-mer in a sequence that gets passed to the resulting decorated
    function.

    Example
    -------

    >>> def count_kmers(x, args): return args+1
    >>> process_kmers(3, False)(count_kmers)(np.array([i for i in b"GATTACA"], dtype=np.uint8), 0)
    5

    The functions of the class:

        * __init__ - Initializes the processor with the length of the k-mers it is goint to be used with as well as the flag that indicates whether the reverse complement of a k-mer should be used as well.
        * __call__ - Calls the class which can be used to decorate a function with the process k-mer functionality.
    """

    def __init__(self, k: int, include_rev_compl: bool = False):
        """Initializes the processor with the length of the k-mers it is goint to be used with
        as well as the flag that indicates whether the reverse complement of a k-mer should be
        used as well.

        Parameters
        ----------
        k : int
            The length of the k-mers the function f is going to be called with.
        include_rev_compl : bool = False
            Flag that indicates whether to also call the function f for the reverse complement
            of the k-mers.
        """

        self.k = k
        self.include_rev_compl = include_rev_compl

    def __call__(self, f: Callable[[int, T], T]) -> Callable[[np.ndarray, T], T]:
        """Calls the class which can be used to decorate a function with the process k-mer functionality.

        Parameters
        ----------
        f : Callable[[int, T], T]
            The function that is going to be called for every k-mer.

        Returns
        -------
        Callable[ [np.ndarray, T], T]
            The decorated function resulting from the call.
        """

        return make_kmer_processor(f, self.k, self.include_rev_compl)


@njit(cache=True)
def _decode(c: int, seq: np.ndarray, k: int, mapping_dict):
    """Decodes the passed in integer encoded k-mer using the mapping_dict and returns it as a np.ndarray.

    Parameters
    ----------
    c: int
        The integer encoded dna-sequence.
    seq: np.ndarray
        The array that the result of the decoding is going to be stored in.
    k: int
        The length of the sequence that is supposed to be decoded into an np.ndarray.
    mapping_dict
        The dictionary that maps the 4 2-bit combinations onto the four characters as bytes for the np.ndarray decoding.
    Returns
    -------
    np.ndarray
        The decoded np.ndarray version of the dna-sequence that was passed in via c.
    """
    for i in range(k):
        seq[k - i - 1] = mapping_dict[c & 0b_11]
        c = c >> 2
    return seq


def dna_decode(c: int, k: int) -> str:
    """Decodes an integer to a DNA sequence as a readable string.

    Examples
    --------

    >>> dna_decode(9156, 7)
    'GATTACA'

    Parameters
    ----------
    c : int
        The integer encoded dna-sequence.
    k : int
        The length of the sequence that is supposed to be decoded into a string.

    Returns
    -------
    str
        The decoded string version of the dna-sequence that vas passed in via c.
    """
    sup_c = 1 << 2 * k  # this is 2**(2*k). maximum bit length: 2*k
    if sup_c <= c:
        raise ValueError(
            f"Argument k is too small for given code c. k: {k} c: {c} maximum c: {sup_c - 1}"
        )

    mapping_dict = np.array([ord(c) for c in "ACGT"], dtype=np.uint8)

    seq = np.zeros(k, dtype=np.uint8)
    return bytes(_decode(c, seq, k, mapping_dict)).decode()
