"""Low-Complexity-ArgParser File
Offers functionalty to generate a user-friendly command-line interface.

This file can contains the following
functions:

    * get_argument_parser - Generates and returns an argument parser. Defines what arguments are required and which types they must have.

Three sub-commands were created
    * process
    * statistics
    * visualize

This file requires 'argparse' to be installed on the device it is going
to be used on.
"""

import argparse
from lcfilter.process import main as main_process
from lcfilter.visualize import main as main_visualize
from lcfilter.statistics import main as main_statistics


def get_argument_parser():
    """Creates the argument parser for the low-complexity-filter tool.

    Returns
    -------
    ArgumentParser
    """

    description = """LCFilter

    LCFilter is a tool for filtering DNA reads by their complexities.
    It can read a FASTQ file (optionally with its paired file) and calculate each contained read's complexity score
    based on the distribution of its k-mers (subsequences of length k).

    The most important subcommand for filtering reads is `process`.
    Subcommands `statistics` and `visualize` can be used to get more information on complexity score distributions.
    """

    p = argparse.ArgumentParser(
        description=description,
        epilog="by project group GeCoCoo, TU Dortmund University.",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    subparsers = p.add_subparsers(help="Sub-command help. A sub-command is required.")

    ### SUBCOMMAND PROCESS
    process_help = """Processes given fastq files by calculating their complexities and identifying them as high or low complexity reads.

Low complexity Reads are written to low.fq with the given output prefix.
High complexity Reads are written to high.fq with the given out prefix.

In the case of paired-end files the output is divided into low1.fq and low2.fq / high1.fq 
and high2.fq respectively, of course with the output prefix.
With paired-end files, there are also output files mixed1.fq and mixed2.fq. 
These contain reads that are classified as mixed complexity.
Reads in mixed1.fq come from the first input file, reads in mixed2.fq come from the second input file.
    """

    output_help = """Prefix for output files. This may be just a prefix, e.g. `out_` or a path `output/` or both `output/out_`."""
    threshold_help = """Number between 0 and 100, except when the filter is set to 'uniques' (defaut) when the minimum value is -125.
    This has different meanings when passed to different filters. When passed to 'uniques'"""
    revcomp_help = """Flag for including reverse complements in complexity calculations.
Reverse complements will be treated as additional k-mers."""
    filter_help = """Which filter to use. Options are `uniques`, `dust` or `entropy`. Standard is `uniques`"""

    parser_process = subparsers.add_parser(
        "process", help=process_help, formatter_class=argparse.RawTextHelpFormatter
    )
    parser_process.add_argument(
        "--fastq", "-q", metavar="FASTQ", help="FASTQ file to process", required=True
    )
    parser_process.add_argument("-o", "--out", help=output_help, default="")
    parser_process.add_argument(
        "-T",
        "--threadcount",
        metavar="INT",
        type=int,
        default=8,
        help="maximum number of threads for calculating and classifying k-mers",
    )
    parser_process.add_argument(
        "-k",
        "--kmer",
        metavar="INT",
        type=int,
        default=5,
        help="length k of k-mers",
    )
    parser_process.add_argument(
        "-t",
        "--threshold",
        metavar="INT",
        type=int,
        default="15",
        help=threshold_help,
    )
    parser_process.add_argument(
        "-p", "--pairs", metavar="FASTQ", help="paired end FASTQ file"
    )
    parser_process.add_argument(
        "-R",
        "--rev",
        action="store_true",
        help=revcomp_help,
    )
    parser_process.add_argument("-f", "--filter", default="uniques", help=filter_help)
    parser_process.set_defaults(func=main_process)

    ### SUBCOMMAND STATISTICS
    statistics_help = """Instead of classifying given Reads, calculates values used for filtering. These are:
max_kmer: Ratio of the amount of the most frequent k-mer to the total amount of k-mers.
unique_kmers: Ratio of the amount of unique k-mers to the total amount of k-mers.
These values are written to a pickle file, which can be visualized by subcommand `visualize`."""
    parser_statistics = subparsers.add_parser(
        "statistics",
        help=statistics_help,
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser_statistics.add_argument(
        "--fastq", "-q", metavar="FASTQ", help="FASTQ file to statistics", required=True
    )
    parser_statistics.add_argument("-o", "--out", help=output_help, default=".")
    parser_statistics.add_argument(
        "-T",
        "--threadcount",
        metavar="INT",
        type=int,
        default=8,
        help="maximum number of threads for calculating and classifying k-mers",
    )
    parser_statistics.add_argument(
        "-k",
        "--kmer",
        metavar="INT",
        type=int,
        default=5,
        help="length k of k-mers",
    )
    parser_statistics.add_argument("-p", "--pairs", help="paired end FASTQ file")
    parser_statistics.add_argument(
        "-R",
        "--rev",
        action="store_true",
        help=revcomp_help,
    )
    parser_statistics.add_argument(
        "-j",
        "--json",
        action="store_true",
        help="the output will be given as json format instead of a pickle object. This will massivley increase the memory usage but remains human-readable.",
    )
    parser_statistics.set_defaults(func=main_statistics)

    ### SUBCOMMAND VISUALIZE
    visualize_help = """Visualizes results produced by the `statistics` subcommand.
The result are seven .png files."""
    parser_visualize = subparsers.add_parser(
        "visualize", help=visualize_help, formatter_class=argparse.RawTextHelpFormatter
    )
    parser_visualize.add_argument(
        "--stats", "-s", metavar="Stats", help="Pickle file to visualize", required=True
    )
    parser_visualize.add_argument(
        "-j",
        "--json",
        action="store_true",
        help="the statistics are taken in from a json file instead of a pickle file. This will massivley increase the memory usage.",
    )
    parser_visualize.add_argument("-o", "--out", help=output_help, default=".")
    parser_visualize.set_defaults(func=main_visualize)

    return p


def main():
    p = get_argument_parser()
    args = p.parse_args()

    if hasattr(args, "func"):
        args.func(args)
    else:
        p.print_help()


if __name__ == "__main__":
    main()
