""" FASTQ Processing File
Offers functionalty for generating a FASTQ processing function. 
This is possible on Single-Read and Paired-End FASTQ files, it offers multithreading and chunkwise reading and
processing of FASTQ files.

This file requires 'numpy' and 'numba' to be installed on the device it is going
to be used on.

This file can be imported by another .py file and contains the following
functions:

    * make_process_read_from_fastq - Creates a function for reading and processing FASTQ files. Users may decide, which output format of processing steps is needed.

"""

import numpy as np
from numba import njit, jit, uint32, prange
import datetime
import io
import contextlib

from concurrent.futures import ThreadPoolExecutor, as_completed
from math import ceil

from lcfilter.kmer import process_kmers
from lcfilter.dnaio import fastq_chunks, fastq_chunks_paired
from lcfilter.filter import filter_factory


DEBUG = True


def make_process_read_from_fastq(
    path,
    threads,
    pairs,
    k,
    rev_compl,
    threshold,
    filter_id="",
    bufsize=2 ** 23,
    chunkreads=2 ** 23 // 200,
):
    """Generate a process function for FASTQ file reading.

    Parameters
    ----------
    path : String
        Commandline Argument to set the output path and name
    threads : int
        Number of Threads
    pairs : file
        Second input file which is given as Commandline Argument
    k : int
        Length of each k-mer, which is used to specify low or high complexity
    rev_compl : Bool
        Whether the reverse complement of each k-mer should also be considered
    threshold : Float
        Number between 0 and 1. Higher numbers will filter out more Reads as low-complexity
    bufsize : int
        Size of created buffer, which will contain the Reads
    chunkreads : int
        Maximum number of Reads, which can be stored in the buffer
    Returns
    -------
    Generated function.
    """

    class dummycontext:
        """ Dummy context to fill unused output writers """

        def __enter__(self):
            return self

        def __exit__(self, *exc):
            return False

        def write(*_):
            pass

        def flush(*_):
            pass

    @contextlib.contextmanager
    def createOutputWriter(path, paired, needed, suffix):
        """
        Creates a BufferedWriter to write the filtered Reads into different output files

        Parameters
        ----------
        path : String
            Commandline Argument to set the output path and name
        paired : Bool
            To check if paired input is used or not
        needed: Bool
            Determines if this writer is needed for single input
        suffix : String
            Name of the file to write to

        Returns
        -------
        BufferedWriter to output filtered reads while processing.
        """
        if path:
            if path[-1] != "/":
                path = path + "_"
        if paired:
            yield io.BufferedWriter(io.FileIO(path + suffix, "w"))
        elif needed:
            suffix = suffix.split("_")[0] + ".fq"
            yield io.BufferedWriter(io.FileIO(path + suffix, "w"))
        else:
            yield dummycontext()

    @njit(nogil=True)
    def get_border(linemarks, threads):
        """Finds border-indices for each chunk depending on the number of used threads

        Parameters
        ----------
        linemarks : 2-dimensional numpy array of type uint32
            Array containing the start- and end-indices of each Read.
            - linemarks[i][0] Start index of the i-th sequence
            - linemarks[i][1] End index of the i-th sequence
            - linemarks[i][2] Start index of the i-th Read
            - linemarks[i][3] End index of the i-th Read
            - e.g. [54, 155, 0, 260] for a sequence of the length 101
        threads : int
            Number of Threads, which determines in how many parts the chunk has to be cut.

        Returns
        -------
        borders : numpy array of type uint32
            Contains the start- and end-indices for each Thread. The end-index of Thread i is the start-index of Thread i+1.
            - e.g. [  0,  17,  34,  51,  68,  85, 102, 119, 136, 153, 170, 187, 200] for 12 Threads (13 indices)
            - The first thread processes the first 17 of 200 reads/sequences
        """
        borders = np.empty(threads + 1, dtype=np.uint32)
        for i in range(threads):
            borders[i] = min(ceil(linemarks.shape[0] / threads) * i, linemarks.shape[0])
        borders[threads] = linemarks.shape[0]
        return borders

    @njit
    @process_kmers(k=k, include_rev_compl=rev_compl)
    @njit(nogil=True)
    def count_low_k(kmer: int, counter: np.ndarray):
        """Function to be called for each k-mer in a sequence to count the occurrence of each k-mer by the use of
        a decorator.

        Parameters
        ----------
        k : int
           Length of each k-mer.
        include_rev_compl : Bool
            Flag that indicates whether the reverse complement of each k-mer shall be also counted.
        kmer: int
            Encoded representation of the seen k-mer, which shall be counted.
        counter : numpy ndarray
            Array containing the number of each k-mer occurring at the index of the integer encoded k-mer.

        Returns
        -------
        counter : numpy ndarray
            Array containing the number of each k-mer occurring at the index of the integer encoded k-mer.
        """
        counter[kmer] += 1
        return counter

    filter_function = filter_factory(filter_id)

    @njit(nogil=True)
    def processor(
        buf,
        linemarks,
        buf1=None,
        linemarks1=None,
    ):
        """Function for processing of each chunk.

        Parameters
        ----------
        buf : numpy array
            Contains all characters of the first input file as byte.
        linemarks : 2-dimensional numpy array of type uint32
            Array containing the start- and end-indices of each read of the first input file.
            - linemarks[i][0] Start index of the i-th sequence
            - linemarks[i][1] End index of the i-th sequence
            - linemarks[i][2] Start index of the i-th Read
            - linemarks[i][3] End index of the i-th Read
            - e.g. [54, 155, 0, 260] for a sequence of the length 101
        out1 : io.BufferedWriter
            BufferedWriter to output each high complexity Read of the first input file.
        out2 : io.BufferedWriter
            BufferedWriter to output each low complexity Read of the first input file.
        buf [optional] : numpy array
            Contains all characters of the second input file (if existing) as byte.
        linemarks1 [optional] : 2-dimensional numpy array of type uint32
            Array containing the start- and end-indices of each Read of the second input file (if existing).
            See linemarks for more information.
        out3 [optional] : io.BufferedWriter
            BufferedWriter to output each high complexity Read of the second input file (if existing).
        out4 [optional] : io.BufferedWriter
            BufferedWriter to output each low complexity Read of the second input file (if existing).
        out5 [optional] : io.BufferedWriter
            BufferedWriter to output each mixed complexity Read of the first input file (if a second input file exists).
        out6 [optional] : io.BufferedWriter
            BufferedWriter to output each mixed complexity Read of the second input file (if existing).

        Returns
        -------
        Numpy array containing the occurrence of each possible k-mer in this chunk.
        """
        n = linemarks.shape[0]
        lowOrHigh = np.zeros(n, dtype=np.uint8)
        for i in range(n):
            sq = buf[linemarks[i, 0] : linemarks[i, 1]]
            counter = np.zeros((4 ** k), dtype=np.uint32)
            counter = count_low_k(sq, args=counter)
            is_high_complexity = filter_function(counter, threshold)

            # processing read of second file
            if not buf1 is None:
                sq1 = buf1[linemarks1[i, 0] : linemarks1[i, 1]]
                counter1 = np.zeros((4 ** k), dtype=np.uint32)
                counter1 = count_low_k(sq1, args=counter1)
                is_high_complexity1 = filter_function(counter1, threshold)

            # write to output for paired files, depending on both complexities
            if not buf1 is None:
                if is_high_complexity and is_high_complexity1:
                    lowOrHigh[i] = 0
                elif is_high_complexity or is_high_complexity1:
                    lowOrHigh[i] = 2  # mixed complexity
                else:
                    lowOrHigh[i] = 1

            # write to output for single file
            else:
                if is_high_complexity:
                    lowOrHigh[i] = 0
                else:
                    lowOrHigh[i] = 1
        return (lowOrHigh, linemarks, linemarks1)

    def process_read_from_fastq(fastq):
        """Process function for FASTQ file reading.

        Parameters
        ----------
        fastq : file
            First input file which is given as Commandline Argument

        Returns
        -------
        Numpy array containing the occurrence of each possible k-mer.
        """
        if pairs:
            paired = True
        else:
            paired = False
        with ThreadPoolExecutor(max_workers=threads) as executor, createOutputWriter(
            path, paired, True, "high_1.fq"
        ) as out1, createOutputWriter(
            path, paired, True, "low_1.fq"
        ) as out2, createOutputWriter(
            path, paired, False, "high_2.fq"
        ) as out3, createOutputWriter(
            path, paired, False, "low_2.fq"
        ) as out4, createOutputWriter(
            path, paired, False, "mixed_1.fq"
        ) as out5, createOutputWriter(
            path, paired, False, "mixed_2.fq"
        ) as out6:

            """Reading input files
            depending on the number of input files (1 or 2 files) a reading method is called
            as many chunks as needed to store the input files are created and processed afterwards
            a chunk is a tuple of two arrays
            - the first contains all characters of the file as byte
            - the second is an array with 4 indices:
                - [start of sequence, end of sequence, start of whole entry, end of whole entry]
                - e.g. [ 54, 155, 0, 260] for the first entry of our FASTQ-file with a sequence-length of 101
            """
            if pairs:
                for chunk in fastq_chunks_paired(
                    (fastq, pairs),
                    bufsize=bufsize * threads,
                    maxreads=chunkreads * threads,
                ):
                    borders = get_border(chunk[1], threads)
                    futures = [
                        executor.submit(
                            processor,
                            chunk[0],
                            chunk[1][borders[i] : borders[i + 1]],
                            chunk[2],
                            chunk[3][borders[i] : borders[i + 1]],
                        )
                        for i in range(threads)
                    ]
                    """ Handling of each result """
                    for i in as_completed(futures):
                        (lowOrHigh, linemarks, linemarks1) = i.result()
                        for seq in range(linemarks.shape[0]):
                            if lowOrHigh[seq] == 0:
                                out1.write(
                                    chunk[0][linemarks[seq][2] : linemarks[seq][3]]
                                )
                                out3.write(
                                    chunk[2][linemarks1[seq][2] : linemarks1[seq][3]]
                                )
                            elif lowOrHigh[seq] == 2:  # mixed complexity
                                out5.write(
                                    chunk[0][linemarks[seq][2] : linemarks[seq][3]]
                                )
                                out6.write(
                                    chunk[2][linemarks1[seq][2] : linemarks1[seq][3]]
                                )
                            else:
                                out2.write(
                                    chunk[0][linemarks[seq][2] : linemarks[seq][3]]
                                )
                                out4.write(
                                    chunk[2][linemarks1[seq][2] : linemarks1[seq][3]]
                                )
            else:
                for chunk in fastq_chunks(
                    fastq, bufsize=bufsize * threads, maxreads=chunkreads * threads
                ):
                    borders = get_border(chunk[1], threads)
                    futures = [
                        executor.submit(
                            processor,
                            chunk[0],
                            chunk[1][borders[i] : borders[i + 1]],
                        )
                        for i in range(threads)
                    ]
                    """ Handling of each result """
                    for i in as_completed(futures):
                        (lowOrHigh, linemarks, linemarks1) = i.result()
                        for seq in range(linemarks.shape[0]):
                            if lowOrHigh[seq] == 0:
                                out1.write(
                                    chunk[0][linemarks[seq][2] : linemarks[seq][3]]
                                )
                            else:
                                out2.write(
                                    chunk[0][linemarks[seq][2] : linemarks[seq][3]]
                                )
        return

    return process_read_from_fastq


def main(args):
    """ Main method for processing Reads """

    now = datetime.datetime.now()
    print(f"#{now:%Y-%m-%d %H:%M:%S}: Begin processing")
    if args.fastq:
        if args.out:
            if args.out[-1] == "/":
                output_folder = args.out
                output_path = args.out
            elif "/" in args.out:
                output_folder = args.out.split("/")[0] + "/"
                output_path = args.out
            else:
                output_folder = ""
                output_path = args.out
        else:
            output_folder = ""
            output_path = args.out

        threshold = min(100, max(0, int(args.threshold))) / 100
        if args.filter == "uniques":
            threshold = min(100, max(-125, int(args.threshold))) / 100

        process_read_from_fastq = make_process_read_from_fastq(
            output_path,
            args.threadcount,
            args.pairs,
            args.kmer,
            args.rev,
            threshold,
            args.filter,
        )
        process_read_from_fastq(args.fastq)
        now = datetime.datetime.now()
        print(f"#{now:%Y-%m-%d %H:%M:%S}: All Reads processed")
    else:
        assert False
