# LC Filter

LCFilter is a tool for filtering DNA reads by their complexities.
It can read a FASTQ file (optionally with its paired file) and calculate each contained read's complexity score
based on the distribution of its k-mers (subsequences of length k).

The most important subcommand for filtering reads is `process`.
Subcommands `statistics` and `visualize` can be used to get more information on complexity score distributions.

# Installation
**How to install lcfilter:**

***Mac/Linux:***

* Install Anaconda or miniconda
* `conda env create -f environment.yml` (Might throw error message, because shell isn’t initialized: `conda init —help`)
* Create folder „raw“ for FASTQ Files.
* For example place SRR9130495_1.fastq, SRR9130495_2.fastq in it ([Download Option](https://www.ebi.ac.uk/ena/browser/view/SRR9130495))
* Create folder „output“ 

To start the tool:
* `conda activate lcfilter`
* `python setup.py develop`

With Snakemake:
* `snakemake --cores all`

Or via shell command:
* `lcfilter process -q path_to_file -o output/`
* use `lcfilter -h` or `lcfilter process -h` for more information on how to use the tool

***Windows:***

* Install Anaconda or miniconda
* `conda create -n lcfilter python=3.8 numpy numba matplotlib`
* Create folder „raw“ for FASTQ Files.
* For example place SRR9130495_1.fastq, SRR9130495_2.fastq in it ([Download Option](https://www.ebi.ac.uk/ena/browser/view/SRR9130495))
* Create folder „output“ 

To start the tool:
* `conda activate lcfilter`
* `python setup.py develop`
* `lcfilter process -q path_to_file -o output/`
* use `lcfilter -h` or `lcfilter process -h` for more information on how to use the tool

# Subcommands

## Process
Processes given fastq files by calculating their complexities and identifying them as high or low complexity reads.

Low complexity Reads are written to low.fq with the given output prefix.
High complexity Reads are written to high.fq with the given out prefix.

In the case of paired-end files the output is divided into low1.fq and low2.fq / high1.fq 
and high2.fq respectively, of course with the output prefix.
With paired-end files, there are also output files mixed1.fq and mixed2.fq. 
These contain reads that are classified as mixed complexity.
Reads in mixed1.fq come from the first input file, reads in mixed2.fq come from the second input file.

## Statistics
Mainly used for development.

Instead of classifying given Reads, calculates values used for filtering. These are:
max_kmer: Ratio of the amount of the most freqent k-mer to the total amount of k-mers.
unique_kmers: Ratio of the amount of unique k-mers to the total amount of k-mers.
These values are written to a pickle file which can be visualized by subcommand `visualize`.

## Visualize
Mainly used for development.

Visualizes results produced by the `statistics` subcommand.
The result are seven .png files.

## Documentation

Check out the documentation for further information on the project.

[Documentation](https://gecocoo.gitlab.io/low-complexity-filtering/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
