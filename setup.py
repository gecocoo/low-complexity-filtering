# coding: utf-8
NAME = "lcfilter"
PACKAGE_NAME = "lcfilter"

import sys

try:
    from setuptools import setup
except ImportError:
    print(f"Please install setuptools before installing {NAME}.", file=sys.stderr)
    exit(1)

if sys.version_info < (3, 6):
    print(f"At least Python 3.6 is required for {NAME}.", file=sys.stderr)
    exit(1)


# load and set VERSION and DESCRIPTION
vcontent = open(f"{PACKAGE_NAME}/_version.py").read()
exec(vcontent)

setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    zip_safe=False,
    license="MIT",
    url="None",
    packages=[PACKAGE_NAME],
    entry_points={
        "console_scripts": [
            f"{NAME} = {PACKAGE_NAME}.__init__:main",
        ],
    },
    package_data={"": ["*.css", "*.sh", "*.html"]},
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Bio-Informatics",
    ],
)
