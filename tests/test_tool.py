import subprocess
import os

"""
Integration tests for low-complexity filter tool
"""


def test_complexity_filter_single_end():
    # setup and execute lc filter
    os.system("python3 setup.py develop")

    # check if process finished with exit 0
    assert (
        os.system("lcfilter process -T 4 -k 6 --fastq tests/data/shortExample.fastq")
        == 0
    )


def test_complexity_filter_paired_end():
    # setup and execute lc filter
    os.system("python3 setup.py develop")

    # check if process finished with exit 0
    assert (
        os.system(
            "lcfilter process -T 4 -k 6 --fastq tests/data/shortExample.fastq --pairs tests/data/shortExample_2.fastq"
        )
        == 0
    )


def test_low_complexity_classifying():
    # setup and execute lc filter
    os.system("python3 setup.py develop")
    os.system("lcfilter process --fastq tests/data/low_complexity.fq -o name")

    low = "name_low.fq"
    high = "name_high.fq"
    low_expected = "tests/data/low_complexity.fq"

    lines_low = open(low, "r").readlines()
    lines_low_expected = open(low_expected, "r").readlines()
    lines_high = open(high, "r").readlines()

    # sort lines to compare generated files with expected files
    lines_low.sort()
    lines_low_expected.sort()

    first = ""
    # check if high contains a read
    if len(lines_high) >= 4:
        first = lines_high[3][0:40]

    # check if all reads are classified as low complexity and none as high complexity
    # misclassification can be trigged by None or multiple empty lines in the test file
    assert lines_low == lines_low_expected and len(lines_high) == 0, (
        "LC misclassified: " + first
    )


def test_high_complexity_classifying():
    # setup and execute lc filter
    os.system("python3 setup.py develop")
    os.system("lcfilter process --fastq tests/data/high_complexity.fq")

    low = "low.fq"
    high = "high.fq"
    high_expected = "tests/data/high_complexity.fq"

    lines_low = open(low, "r").readlines()
    lines_high_expected = open(high_expected, "r").readlines()
    lines_high = open(high, "r").readlines()

    # sort lines to compare generated files with expected files
    lines_high.sort()
    lines_high_expected.sort()

    # check if high contains a read
    first = ""
    if len(lines_low) >= 4:
        first = lines_low[3][0:40]

    # check if all reads are classified as low complexity and none as high complexity
    # misclassification can be trigged by None or multiple empty lines in the test file
    assert lines_high == lines_high_expected and len(lines_low) == 0, (
        "HC misclassified: " + first
    )


def test_read_shorter_than_k_mer():
    # setup and execute lc filter
    os.system("python3 setup.py develop")

    # os.system() returns 0 if everything is ok
    # 256 means the spawned program terminated with exit code which should not be the case
    assert (
        os.system("lcfilter process -T 4 -k 6 --fastq tests/data/short_reads.fq") == 0
    )


def test_fastq_file_with_single_read_with_newline():
    # setup and execute lc filter
    os.system("python3 setup.py develop")

    # os.system() returns 0 if everything is ok
    # 256 means the spawned program terminated with exit code which should not be the case
    assert (
        os.system("lcfilter process -T 4 -k 6 --fastq tests/data/single_read.fq") == 0
    )


def test_fastq_file_with_single_read_without_newline():
    # setup and execute lc filter
    os.system("python3 setup.py develop")

    # os.system() returns 0 if everything is ok
    # 256 means the spawned program terminated with exit code which should be the case
    assert (
        os.system(
            "lcfilter process -T 4 -k 6 --fastq tests/data/single_read_without_newline.fq"
        )
        > 0
    )


def test_fastq_file_with_single_read_without_newline_paired():
    # setup and execute lc filter
    os.system("python3 setup.py develop")

    # os.system() returns 0 if everything is ok
    # 256 means the spawned program terminated with exit code which should be the case
    assert (
        os.system(
            "lcfilter process -T 4 -k 6 --fastq tests/data/single_read_without_newline.fq --pairs tests/data/single_read_without_newline.fq"
        )
        > 0
    )


def test_fastq_file_with_multiple_reads_without_newline():
    # setup and execute lc filter
    os.system("python3 setup.py develop")
    lcfilter_output = subprocess.run(
        [
            "lcfilter",
            "process",
            "-T",
            "4",
            "-k",
            "6",
            "--fastq",
            "tests/data/shortExample_without_newline.fastq",
        ],
        capture_output=True,
        text=True,
    ).stderr
    warning = "UserWarning: detected possible data loss, check for cut-off record or missing new line at the end of file"

    assert lcfilter_output.find(warning)


def test_fastq_file_with_multiple_reads_without_newline_paired():
    # setup and execute lc filter
    os.system("python3 setup.py develop")
    lcfilter_output = subprocess.run(
        [
            "lcfilter",
            "process",
            "-T",
            "4",
            "-k",
            "6",
            "--fastq",
            "tests/data/shortExample_without_newline.fastq",
            "--pairs",
            "tests/data/shortExample_without_newline.fastq",
        ],
        capture_output=True,
        text=True,
    ).stderr
    warning = "UserWarning: detected possible data loss, check for cut-off record or missing new line at the end of file"

    assert lcfilter_output.find(warning)


def test_empty_fastq_file():
    # setup and execute lc filter
    os.system("python3 setup.py develop")

    # os.system() returns 0 if everything is ok
    # 256 means the spawned program terminated with exit code which should not be the case
    assert os.system("lcfilter process -T 4 -k 6 --fastq tests/data/empty.fq") == 0


def test_cut_off_fastq_file():
    # setup and execute lc filter
    os.system("python3 setup.py develop")
    lcfilter_output = subprocess.run(
        [
            "lcfilter",
            "process",
            "-T",
            "4",
            "-k",
            "6",
            "--fastq",
            "tests/data/cut_off_read.fq",
        ],
        capture_output=True,
        text=True,
    ).stderr
    warning = "UserWarning: detected possible data loss, check for cut-off record or missing new line at the end of file"

    assert lcfilter_output.find(warning)


def test_broken_format():
    # setup and execute lc filter
    os.system("python3 setup.py develop")

    # os.system() returns 0 if everything is ok
    # > 0 means the spawned program terminated with exit code which should be the case
    assert (
        os.system("lcfilter process -T 4 -k 6 --fastq tests/data/brokenFQFormat.fq") > 0
    )


def test_short_reads_for_low_complexity():
    # setup and execute lc filter
    os.system("python3 setup.py develop")
    os.system("lcfilter process -T 4 -k 6 --fastq tests/data/short_reads2.fq")

    low = "low.fq"
    high = "high.fq"
    low_expected = "tests/data/short_reads2.fq"

    lines_low = open(low, "r").readlines()
    lines_low_expected = open(low_expected, "r").readlines()
    lines_high = open(high, "r").readlines()

    # sort lines to compare generated files with expected files
    lines_low.sort()
    lines_low_expected.sort()

    # check if all reads are classified as low complexity and none as high complexity
    assert lines_low == lines_low_expected and len(lines_high) == 0


def test_no_subcommand():
    os.system("python3 setup.py develop")
    assert os.system("lcfilter") == 0


test_fastq_file_with_multiple_reads_without_newline_paired()
