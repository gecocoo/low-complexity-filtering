"""
A small generator for DNA test data.
"""
import random as rnd
import numpy as np


def random_seq(length):
    """
    Generates a random DNA sequence of given length.
    """
    c = rnd.choices("TCGA", k=length)
    return "".join(c)


def generate_in_subsequences(total_length, length_per_subseq):
    """
    Generates a DNA sequence by repeating a randomly generated subsequence of length length_per_subseq
    until the desired length total_length is reached.
    This offers a simple way of controlling the result's complexity.
    """
    # length_per_subseq = total_length // number_subsequences
    print("subsequence length:", length_per_subseq)
    if length_per_subseq < 1:
        return ""

    result = random_seq(length_per_subseq)
    length_left = total_length - length_per_subseq

    while length_left > 0:
        l_this_round = min(length_left, length_per_subseq)
        result += result[:l_this_round]
        length_left -= l_this_round

    return result


if __name__ == "__main__":
    s = generate_in_subsequences(101, 100)
    print(s)
