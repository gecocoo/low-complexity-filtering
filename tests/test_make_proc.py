"""
Demonstration of make_kmer_processor
"""
import numpy as np
import pytest
from lcfilter.kmer import dna_encode, make_kmer_processor, process_kmers


@pytest.fixture
def seq_arr():
    seq = b"ATGGAGAGCCTTGTCCCTGGTTTCAACGAGAAAACACACGTCCAACTCAGTTTGCCTGTTTTACAG"
    return np.frombuffer(seq, np.uint8)


def test_gc_seq_arr(seq_arr):
    assert bytes(seq_arr).upper().count(b"GC") == 2


def test_gc_counter(seq_arr):
    gc_kmer = dna_encode("GC")
    assert gc_kmer == 0b_10_01

    def count_gc_update(kmer: int, counter: int):
        return counter + int(kmer == gc_kmer)

    count_gc = make_kmer_processor(count_gc_update, k=2, include_rev_compl=False)
    num_gc = count_gc(seq_arr, args=0)
    assert num_gc == 2


def test_gc_counter_decorator(seq_arr):
    gc_kmer = dna_encode("GC")

    @process_kmers(k=2)
    def count_gc(kmer: int, counter: int):
        return counter + int(kmer == gc_kmer)

    assert count_gc(seq_arr, args=0) == 2
