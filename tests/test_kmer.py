import os
import sys
import numpy as np

# sys.path.insert(0,
#                 os.path.abspath(os.path.join(os.path.dirname(__file__), '../kmer/')))
from lcfilter.kmer import (
    dna_decode,
    make_kmer_processor,
    rev_compl,
    comp,
    create_byte_rev_table,
    process_kmers,
)
from lcfilter.dna_encode import dna_encode
import pytest
from collections import Counter
from numba import njit
import numpy as np


def test_dnadecode_act():
    assert dna_decode(0b000111, k=3) == "ACT"


def test_dnadecode_gattaca():
    assert dna_decode(0b10001111000100, 7) == "GATTACA"


def test_dnadecode_gattaca_leading_as():
    assert dna_decode(0b10001111000100, 7 + 3) == "AAA" + "GATTACA"


def test_dna_decode_error():
    with pytest.raises(ValueError):
        dna_decode(123, 3)


def test_create_byte_rev_table():
    table = create_byte_rev_table()
    assert table.size == 256


# def test_get_kmers_str():
#     seq = "GATTACA"
#     c = get_kmers_str(seq, 3)
#     c2 = Counter(GAT=1, ATT=1, TTA=1, TAC=1, ACA=1)
#     assert c == c2


# def test_get_kmers():
#     k = 3
#     seq = np.array([i for i in b"GATTACA"], dtype=np.uint8)
#     c = get_kmers(seq, k)
#     c2 = np.zeros(4 ** k, dtype=np.uint32)
#     c2[dna_encode("GAT")] = 1
#     c2[dna_encode("ATT")] = 1
#     c2[dna_encode("TTA")] = 1
#     c2[dna_encode("TAC")] = 1
#     c2[dna_encode("ACA")] = 1
#     assert np.array_equal(c, c2)


# def test_get_kmers_with_rev_compl():
#     k = 3
#     seq = np.array([i for i in b"GATTACA"], dtype=np.uint8)
#     c = get_kmers(seq, k, True)
#     c2 = np.zeros(4 ** k, dtype=np.uint32)
#     c2[dna_encode("GAT")] = 1
#     c2[dna_encode("ATT")] = 1
#     c2[dna_encode("TTA")] = 1
#     c2[dna_encode("TAC")] = 1
#     c2[dna_encode("ACA")] = 1
#     c2[dna_encode("ATC")] = 1  # reverse complement of GAT
#     c2[dna_encode("AAT")] = 1  # reverse complement of ATT
#     c2[dna_encode("TAA")] = 1  # reverse complement of TTA
#     c2[dna_encode("GTA")] = 1  # reverse complement of TAC
#     c2[dna_encode("TGT")] = 1  # reverse complement of ACA
#     assert np.array_equal(c, c2)


def test_comp():
    c1 = dna_encode("GATTACA")
    c2 = dna_encode("CTAATGT")
    r = comp(c1, 7)
    assert c2 == r


def test_comp2():
    c1 = dna_encode("GATTACAG")
    c2 = dna_encode("CTAATGTC")
    r = comp(c1, 8)
    assert c2 == r


def test_rev_compl():
    s1 = dna_encode("GATAC")
    s2 = dna_encode("GTATC")
    r = rev_compl(s1, 5)
    assert s2 == r


def test_make_kmer_processor_for_ACGTATTACGAG_k4():
    # Tests whether the function f gets called the correct number of times
    # with the correct kmer each time:
    correct_kmers = [
        0b00_01_10_11,  # ACGT
        0b01_10_11_00,  # CGTA
        0b10_11_00_11,  # GTAT
        0b11_00_11_11,  # TATT
        0b00_11_11_00,  # ATTA
        0b11_11_00_01,  # TTAC
        0b11_00_01_10,  # TACG
        0b00_01_10_00,  # ACGA
        0b01_10_00_10,  # CGAG
    ]

    def check_arguments_for_f(kmer, args):
        assert kmer == correct_kmers[args]
        args += 1

        return args

    # Creates a sequence and encodes it:
    seq = "ACGTATTACGAG"
    arr = np.frombuffer(seq.encode(), dtype=np.uint8)

    # Makes the processor:
    processor = make_kmer_processor(check_arguments_for_f, 4, include_rev_compl=False)

    # Calls the processor:
    processor(arr, 0)


def test_make_kmer_processor_for_ACGTATTACGAG_k4_with_rev_compl():
    # Tests whether the function f gets called the correct number of times
    # with the correct kmer each time:
    correct_kmers = [
        0b00_01_10_11,  # ACGT
        0b00_01_10_11,  # ACGT(reverse complement of ACGT)
        0b01_10_11_00,  # CGTA
        0b11_00_01_10,  # TACG(reverse complement of CGTA)
        0b10_11_00_11,  # GTAT
        0b00_11_00_01,  # ATAC(reverse complement of GTAT)
        0b11_00_11_11,  # TATT
        0b00_00_11_00,  # AATA(reverse complement of TATT)
        0b00_11_11_00,  # ATTA
        0b11_00_00_11,  # TAAT(reverse complement of ATTA)
        0b11_11_00_01,  # TTAC
        0b10_11_00_00,  # GTAA(reverse complement of TTAC)
        0b11_00_01_10,  # TACG
        0b01_10_11_00,  # CGTA(reverse complement of TACG)
        0b00_01_10_00,  # ACGA
        0b11_01_10_11,  # TCGT(reverse complement of ACGA)
        0b01_10_00_10,  # CGAG
        0b01_11_01_10,  # CTCG(reverse complement of CGAG)
    ]

    def check_arguments_for_f(kmer, args):
        assert kmer == correct_kmers[args]
        args += 1

        return args

    # Creates a sequence and encodes it:
    seq = "ACGTATTACGAG"
    arr = np.frombuffer(seq.encode(), dtype=np.uint8)

    # Makes the processor:
    processor = make_kmer_processor(check_arguments_for_f, 4, include_rev_compl=True)

    # Calls the processor:
    processor(arr, 0)


def test_make_kmer_processor_for_TTAGCA_k2():
    # Tests whether the function f gets called the correct number of times
    # with the correct kmer each time:
    correct_kmers = [
        0b11_11,  # TT
        0b11_00,  # TA
        0b00_10,  # AG
        0b10_01,  # GC
        0b01_00,  # CA
    ]

    def check_arguments_for_f(kmer, args):
        assert kmer == correct_kmers[args]
        args += 1

        return args

    # Creates a sequence and encodes it:
    seq = "TTAGCA"
    arr = np.frombuffer(seq.encode(), dtype=np.uint8)

    # Makes the processor:
    processor = make_kmer_processor(check_arguments_for_f, 2, include_rev_compl=False)

    # Calls the processor:
    processor(arr, 0)


def test_make_kmer_processor_for_TTAGCA_k2_with_rev_compl():
    # Tests whether the function f gets called the correct number of times
    # with the correct kmer each time:
    correct_kmers = [
        0b11_11,  # TT
        0b00_00,  # AA(reverse complement of TT)
        0b11_00,  # TA
        0b11_00,  # TA(reverse complement of TA)
        0b00_10,  # AG
        0b01_11,  # CT(reverse complement of AG)
        0b10_01,  # GC
        0b10_01,  # GC(reverse complement of GC)
        0b01_00,  # CA
        0b11_10,  # TG(reverse complement of CA)
    ]

    def check_arguments_for_f(kmer, args):
        assert kmer == correct_kmers[args]
        args += 1

        return args

    # Creates a sequence and encodes it:
    seq = "TTAGCA"
    arr = np.frombuffer(seq.encode(), dtype=np.uint8)

    # Makes the processor:
    processor = make_kmer_processor(check_arguments_for_f, 2, include_rev_compl=True)

    # Calls the processor:
    processor(arr, 0)


def test_make_kmer_processor_error_on_k_smaller_1():
    # Tests whether the make_kmer_processor function raises an error on k being smaller 1:

    # Creates a sequence and encodes it:
    seq = "TTAGCA"
    np.frombuffer(seq.encode(), dtype=np.uint8)

    with pytest.raises(ValueError):
        make_kmer_processor(None, -1, include_rev_compl=False)


def test_make_kmer_processor_k_longer_than_arr():
    # Tests whether the make_kmer_processor function raises an error on the array being shorter then the length of a kmer:

    # Creates a sequence and encodes it:
    seq = "TTAGCA"
    arr = np.frombuffer(seq.encode(), dtype=np.uint8)
    k = len(seq) + 1

    processor = make_kmer_processor(None, k, False)

    # Calls the processor:
    processor(arr, 0)


def test_process_kmers():
    rev_compl = False
    k = 3

    @njit
    @process_kmers(k=k, include_rev_compl=rev_compl)
    @njit
    def count_low_k(kmer: int, counter: np.ndarray):
        counter[kmer] += 1
        return counter

    counter = np.zeros((4 ** k), dtype=np.uint32)

    sequence = "GATTACA"
    counter = count_low_k(np.frombuffer(sequence.encode(), dtype=np.uint8), counter)

    correct = np.zeros((4 ** k), dtype=np.uint32)
    kmers = ["GAT", "ATT", "TTA", "TAC", "ACA"]
    for m in kmers:
        correct[dna_encode(m)] += 1

    assert (counter == correct).all()
