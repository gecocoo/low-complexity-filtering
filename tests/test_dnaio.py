import numpy as np
from lcfilter.dnaio import fastq_chunks, fastq_chunks_paired, _find_fastq_seqmarks
import pytest

test_file = "tests/data/shortExample.fastq"


def test_fastq_chunks_buffer_too_small():
    with pytest.raises(ValueError):
        for _ in fastq_chunks(test_file, bufsize=0):
            pass


def test_fastq_chunks_buffer_too_large():
    with pytest.raises(ValueError):
        for _ in fastq_chunks(test_file, bufsize=2 ** 31):
            pass


def test_find_fastq_seqmarks():
    bufsize = 0
    maxreads = bufsize // 200
    buf = np.empty(bufsize, dtype=np.uint8)
    linemarks = np.empty((maxreads, 4), dtype=np.int32)
    m, nxt = _find_fastq_seqmarks(buf, linemarks)
    assert m == 0 and nxt == 0
