import os
import sys
import numpy as np
import pytest
from lcfilter.dna_encode import (
    dna_encode,
    _dna_encode_bytes_array,
    _dna_encode_str_if,
    _dna_encode_ndarray_array_2_at_a_time,
)


# sys.path.insert(0,
#                 os.path.abspath(os.path.join(os.path.dirname(__file__), '../kmer')))


def test_dnaencode_act():
    seq = "ACT"
    seq_int = 0b000111
    assert seq_int == dna_encode(seq)


def test_dnaencode_str_gattaca():
    assert dna_encode("GATTACA") == 0b10001111000100


# def test_dna_encode_str_array():
#     assert _dna_encode_str_array("GATTACA") == 0b10001111000100


# def test_dna_encode_str_2_at_a_time_array():
#     assert _dna_encode_str_array_2_at_a_time("GATTACA") == 0b10001111000100


def test_dna_encode_str_if():
    assert _dna_encode_str_if("GATTACA") == 0b10001111000100


def test_dnaencode_bytes_gattaca():
    assert dna_encode(b"GATTACA") == 0b10001111000100


def test_dnaencode_nparray_gattaca():
    assert (
        dna_encode(np.frombuffer("GATTACA".encode(), dtype=np.uint8))
        == 0b10001111000100
    )


def test_dnaencode_bytes_array_gattaca():
    assert _dna_encode_bytes_array(bytes("GATTACA", "utf-8")) == 0b10001111000100


# def test_dnaencode_bytes_array_2_at_a_time_gattaca():
#     assert _dna_encode_bytes_array_2_at_a_time(
#         bytes("GATTACA", 'utf-8')) == 0b10001111000100


# def test_dnaencode_bytes_if_gattaca():
#     assert _dna_encode_bytes_if(bytes("GATTACA", 'utf-8')) == 0b10001111000100


# def test_dnaencode_ndarray_array_gattaca():
#     assert _dna_encode_ndarray_array(
#         np.frombuffer("GATTACA".encode(), dtype=np.uint8)) == 0b10001111000100


def test_dnaencode_ndarray_array_2_at_a_time_gattaca():
    assert (
        _dna_encode_ndarray_array_2_at_a_time(
            np.frombuffer("GATTACA".encode(), dtype=np.uint8)
        )
        == 0b10001111000100
    )


# def test_dnaencode_ndarray_if_gattaca():
#     assert _dna_encode_ndarray_if(
#         np.frombuffer("GATTACA".encode(), dtype=np.uint8)) == 0b10001111000100


def test_dna_encode_wrong_type():
    with pytest.raises(NotImplementedError):
        dna_encode(["A", "G", "T"])
