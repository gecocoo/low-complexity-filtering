"""
Unit tests for filter methods
"""
import numpy as np
from lcfilter.filter import (
    max_kmer,
    unique_kmers,
    filter,
    filter_precomputed,
    max_kmer_inverted,
)

"""
JUST_ONE: Low Complexity
EQUAL_AMOUNTS: High Complexity
"""
COUNTER_JUST_ONE = np.array([0, 0, 0, 0, 0, 0, 0, 0, 15, 0, 0, 0, 0, 0, 0, 0])
COUNTER_EQUAL_AMOUNTS = np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1])

assert len(COUNTER_JUST_ONE) == 16
assert len(COUNTER_EQUAL_AMOUNTS) == 16


# def test_max_kmer_none():
#     input = None
#     result = max_kmer(input)
#     assert result == 1


def test_max_kmer_empty_array():
    input = np.array([])
    result = max_kmer(input)
    assert result == 1


def test_max_kmer_one_kmer():
    input = COUNTER_JUST_ONE
    result = max_kmer(input)
    assert result == 1


def test_max_kmer_same_amounts():
    input = COUNTER_EQUAL_AMOUNTS
    result = max_kmer(input)
    assert result == 0


# def test_unique_kmers_none():
#     input = None
#     result = unique_kmers(input)
#     assert result == 0


def test_unique_kmers_empty_array():
    input = np.array([])
    result = unique_kmers(input)
    assert result == 0


def test_unique_kmers_one_kmer():
    input = COUNTER_JUST_ONE
    result = unique_kmers(input)
    assert result == 0


def test_unique_kmers_same_amounts():
    input = COUNTER_EQUAL_AMOUNTS
    result = unique_kmers(input)
    assert result == 1


def test_filter_same_amounts():
    t = 0.15
    result = filter(COUNTER_EQUAL_AMOUNTS, t)
    # correct = 1 - 0 * 1.25 > t
    assert result


def test_filter_one_kmer():
    t = 0.15
    result = filter(COUNTER_JUST_ONE, t)
    # correct = 0 - 1 * 1.25 > t
    assert not result


def test_filter_precomputed():
    uni = unique_kmers(COUNTER_EQUAL_AMOUNTS)
    m = max_kmer(COUNTER_EQUAL_AMOUNTS)
    t = 0.15
    result = filter_precomputed(uni, m, t)
    # correct = uni - m * 1.25 > t
    assert result


def test_kmer_inverted():
    m = max_kmer(COUNTER_EQUAL_AMOUNTS)
    result = max_kmer_inverted(COUNTER_EQUAL_AMOUNTS)
    assert result == 1 - m
